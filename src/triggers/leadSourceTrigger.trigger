trigger leadSourceTrigger on Lead (Before Update)
{      
    set<string> leadSourceSet = new set<string>();
    if(Trigger.isUpdate)
    {
            for(Lead ld: Trigger.new)
            {
            leadSourceSet.add(ld.LeadSource);
            }           
                       
            for(Lead ld: Trigger.new)
            {
                    if(ld.LeadSource != trigger.oldMap.get(ld.id).LeadSource)
                    {
                    ld.addError('Need to retain the old lead source as' + trigger.oldMap.get(ld.id).LeadSource + 'only' );
                    }              
            }            
    }
    }