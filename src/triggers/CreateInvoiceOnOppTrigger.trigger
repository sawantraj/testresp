trigger CreateInvoiceOnOppTrigger on Opportunity (after update) {
	
	for (Opportunity opportunity : trigger.New)
	if (trigger.OldMap.get(opportunity.id).stageName != opportunity.stageName && opportunity.stageName == 'Closed Won') {
		new OpportunityToXero(opportunity);
	}

}