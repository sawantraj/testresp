/* This trigger is created on opportunity object & will be triggered 
   on after update event.
   
 * Revision History:
 *
 * Version       Date                Author              Comments
 *  1.0        20/2/2013    njr        Initial Draft
*/
trigger CreateContractTrigger on Opportunity (after update)
 {
    CreateContractTriggerHandler ccth = new CreateContractTriggerHandler();
    ccth.onChangeStageUpdate(trigger.new,trigger.oldMap);
}