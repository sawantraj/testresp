trigger AccountTrigger on Account (before insert, before update) 
{
    if(trigger.isBefore){
        
        if(trigger.isInsert){
            List<String> acclist = new List<String>();
        Account accRec = new Account ();
        Map<ID,List<Account>> accMapCount = new Map<ID,List<Account>>();        
        List<Account> accResults = [select Id, Name, Phone, Email__c from Account];
        system.debug('accResults---------'+accResults); 
     if(accResults.size()>0 && accResults!=null)
        {
            for(Integer i=0;i<accResults.size();i++)
            {
                accMapCount.put(accResults[i].Id,accResults);
                system.debug('accMapCount---------'+accMapCount); 
            }
        }
        
        for(Account objAcc:Trigger.new)
        {system.debug('objAcc---------'+objAcc);
                if(String.ValueOf(objAcc.Id)!=null && String.ValueOf(objAcc.Id)!='' )
                {
                    system.debug('accMapCount---------'+accMapCount);
                    List<Account> oldAccount = accMapCount.get(objAcc.Id);
                    system.debug('oldAccount---------'+oldAccount); 
                     if(oldAccount!=null && oldAccount.size()>0)
                     {
                        for(Account objAccnt:oldAccount)
                        {system.debug('objAccnt---------'+objAccnt);
                            
                                if(objAcc.Name!= null && objAcc.Name == objAccnt.Name && objAcc.Phone == objAccnt.Phone && objAcc.Email__c == objAccnt.Email__c )
                                 { 
                                    System.debug('objAcc--02--'+objAcc);
                                    objAcc.Name = objAcc.Name;
                                    objAcc.Phone = objAcc.Phone;                                    
                                    objAcc.Email__c = objAcc.Email__c;
                                    objAcc.ParentId = objAccnt.Id;
                                                                                                      
                                }                                
                                else if(objAcc.Name!= null && objAcc.Email__c == null && objAcc.Phone == null)
                                {
                                    System.debug('objAcc--03--'+objAcc);
                                    objAcc.Name = objAcc.Name;
                                    
                                } 
                                    else if(objAcc.Email__c == null && objAcc.Name == objAccnt.Name && objAcc.Phone == objAccnt.Phone && objAcc.Name!= null)
                                    {
                                        objAcc.Name = objAcc.Name;
                                        objAcc.Phone = objAcc.Phone;
                                        objAcc.ParentId = objAccnt.Id;
                                            
                                        }   
                                    else if(objAcc.Phone == null && objAcc.Email__c == objAccnt.Email__c && objAcc.Name == objAccnt.Name && objAcc.Name!= null )
                                    {
                                        objAcc.Name = objAcc.Name;
                                        objAcc.Email__c = objAccnt.Email__c;
                                        objAcc.ParentId = objAccnt.Id;
                                                                                    
                                     }                                                                                                                                        
                               insert objAcc;                     
                           }                          
                        }                               
                     }         
               }        
            }             
        if(trigger.isUpdate){
            
            
        }
    }
        
}