trigger AccountDupTrigger on Account (before insert, before update) {
	set<string> emailSet = new set<string>();
	set<string> phoneSet = new set<string>();
	set<string> nameSet = new set<string>();
	 
	if(Trigger.isInsert)
	{
		for (Account Acc : trigger.new )
		{
			emailSet.add(Acc.Email__c);
			phoneSet.add(Acc.Phone);
			nameSet.add(Acc.Name);
											
		}	
		List<Account> oldAccountList = [select Id, Name, Phone, Email__c from Account where( Name IN : nameSet OR Phone IN : phoneSet OR Email__c IN : emailSet )]; 
	    System.debug('oldAccountList---'+oldAccountList);
	    
	    
		for(Account Acc:Trigger.new)
		{
			for(Account oldAccount: oldAccountList)
			{
				/* if Name , Email and Phone Number of new record is equal to existing record */
                        if(Acc.Name!= null && Acc.Name == oldAccount.Name && Acc.Phone == oldAccount.Phone && Acc.Phone != null 
                                                                      && Acc.Email__c == oldAccount.Email__c && Acc.Email__c!= null)
                        {    System.debug('Acc----'+Acc);
                            //Acc.addError('Account Already Existttttttttttt');
                            Acc.Name = Acc.Name;
                            Acc.Phone = Acc.Phone;
                            Acc.ParentId = oldAccount.Id;
                            Acc.Email__c = Acc.Email__c;
                        }
                        
                        /* if Email and the Phone Number of the new record are empty */
                        else if(Acc.Name!= null  && Acc.Email__c == null && Acc.Phone == null)
                        {
                            Acc.Name = Acc.Name;                            
                        }
                        
                        /* Phone is empty and Name and Email of the new record is equal to the existing record*/
                        else if(Acc.Phone == null )
	                        {
		                        if(Acc.Name!= null && Acc.Name == oldAccount.Name && Acc.Email__c == oldAccount.Email__c)
		                        {
		                            Acc.Name = Acc.Name;                                                                                                     
		                            Acc.Email__c = Acc.Email__c;
		                            Acc.ParentId = oldAccount.Id;	
		                        }	
	                        }
	                        
	                    /* Email is empty and Name and Phone of the new record is equal to the existing record*/    
                        else if(Acc.Email__c == null)
                        {
	                        if (Acc.Name!= null && Acc.Name == oldAccount.Name && Acc.Phone == oldAccount.Phone )
	                        {
	                        	Acc.Name = Acc.Name;
	                            Acc.Phone = Acc.Phone;
	                            Acc.ParentId = oldAccount.Id;
	                        }	                        	
                        }
			}
		}	
	}
}