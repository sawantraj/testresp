trigger DupTrigger on Account (before insert, before Update) {

    //List<String> acclist = new List<String>();
    //Map<ID,List<SObject>> accMapCount = new Map<ID,List<SObject>>();
    //Account accRec = new Account ();
    
    
    /* 
    	1. Create a set of Email Ids for new Account records - emailSet
    	2. Create a set of Phone for new Account records - phoneSet
    	3. Create a set of Name Ids for new Account records - nameSet
    	4. Query the database for any records that match their values in the above created sets
    	   i.e. Phone in phoneSet OR Email in emailSet OR Name in nameSet
    	5. If any recordcs are returned from the query then iterate and check if atleast 2 out of 
    	   3 field values match
    	6. If match found create a Account record and set newAccount.Parent = oldAccount.Id
    	7. If match not found create an Account record 
    */
    
    List<Account> oldAccount = [select Id, Name,Phone,Email__c from Account];   
       
        if(Trigger.isInsert)
        {           
            for(Account objAcc : Trigger.new)
            {
            System.debug('objAcc--b--'+objAcc);
                if(oldAccount!=null && oldAccount.size()>0)
                {
                    for(Account objAccnt : oldAccount)
                    {
                    if(objAccnt!=null)
                    {
                    	
                    	/* if Name , Email and Phone Number of new record is equal to existing record */
                        if(objAcc.Name!= null && objAcc.Name == objAccnt.Name && objAcc.Phone == objAccnt.Phone && objAcc.Phone != null 
                                                                      && objAcc.Email__c == objAccnt.Email__c && objAcc.Email__c!= null)
                        {    System.debug('objAcc-00---'+objAcc);
                            //objAcc.addError('Account Already Existttttttttttt');
                            objAcc.Name = objAcc.Name;
                            objAcc.Phone = objAcc.Phone;
                            objAcc.ParentId = objAccnt.Id;
                            objAcc.Email__c = objAcc.Email__c;
                        }
                        
                        /* if Email and the Phone Number of the new record are empty */
                        else if(objAcc.Name!= null  && objAcc.Email__c == null && objAcc.Phone == null)
                        {
                            objAcc.Name = objAcc.Name;                            
                        }
                        
                        /* Phone is empty and Name and Email of the new record is equal to the existing record*/
                        else if(objAcc.Phone == null )
	                        {
		                        if(objAcc.Name!= null && objAcc.Name == objAccnt.Name && objAcc.Email__c == objAccnt.Email__c)
		                        {
		                            objAcc.Name = objAcc.Name;                                                                                                     
		                            objAcc.Email__c = objAcc.Email__c;
		                            objAcc.ParentId = objAccnt.Id;	
		                        }	
	                        }
	                        
	                    /* Email is empty and Name and Phone of the new record is equal to the existing record*/    
                        else if(objAcc.Email__c == null)
                        {
	                        if (objAcc.Name!= null && objAcc.Name == objAccnt.Name && objAcc.Phone == objAccnt.Phone )
	                        {
	                        	objAcc.Name = objAcc.Name;
	                            objAcc.Phone = objAcc.Phone;
	                            objAcc.ParentId = objAccnt.Id;
	                        }	                        	
                        }                        
                      }                    
                }
            }
        }
        }
                       
          if(Trigger.isUpdate)
           {
         	for(Account objAcc:Trigger.new)
            {
            if(String.ValueOf(objAcc.Id)!=null && String.ValueOf(objAcc.Id)!='' )
            {
                
                if(oldAccount!=null && oldAccount.size()>0){
                    for(Account objAccnt:oldAccount)
                    {
                       if(objAccnt!=null)
                        {
                           system.debug('objAcc----'+objAcc);
                           
                           /* if Name , Email and Phone Number of new record is equal to existing record */
                           if(objAcc.Name!= null && objAcc.Name == objAccnt.Name && objAcc.Phone == objAccnt.Phone && objAcc.Phone != null 
                                                                         && objAcc.Email__c == objAccnt.Email__c && objAcc.Email__c!= null)
                        {    System.debug('objAcc-00---'+objAcc);
                            objAcc.Name = objAcc.Name;
                            objAcc.Phone = objAcc.Phone;
                            objAcc.ParentId = objAccnt.Id;
                            objAcc.Email__c = objAcc.Email__c;
                        }
                        
                        /* if Email and the Phone Number of the new record are empty */
                        else if(objAcc.Name!= null  && objAcc.Email__c == null && objAcc.Phone == null)
                        {
                            objAcc.Name = objAcc.Name;                            
                        }
                        
                         /* Phone is empty and Name and Email of the new record is equal to the existing record*/
                        else if(objAcc.Phone == null )
	                        {
		                        if(objAcc.Name!= null && objAcc.Name == objAccnt.Name && objAcc.Email__c == objAccnt.Email__c)
		                        {
		                            objAcc.Name = objAcc.Name;
		                            objAcc.Email__c = objAcc.Email__c;
		                            objAcc.ParentId = objAccnt.Id;	
		                        }	
	                        }
	                        
	                     /* Email is empty and Name and Phone of the new record is equal to the existing record*/    
                        else if(objAcc.Email__c == null)
                        {
	                        if (objAcc.Name!= null && objAcc.Name == objAccnt.Name && objAcc.Phone == objAccnt.Phone )
	                        {
	                        	objAcc.Name = objAcc.Name;
	                            objAcc.Phone = objAcc.Phone;
	                            objAcc.ParentId = objAccnt.Id; 
	                        }	                        	
                        }                               
                      }                     
                                             
                      }
                    
                    }
            }
                }
         }            
      }