trigger compositeDuplication on Account (before insert, before Update) {

    List<String> acclist = new List<String>();
    Map<ID,List<SObject>> accMapCount = new Map<ID,List<SObject>>();
    Account accRec = new Account ();
    
        
    
    List<Account> accResults = [select Id, Name,Phone,Email__c from Account];
    system.debug('accResults---------'+accResults);
    if(accResults.size()>0 && accResults!=null)
    {
        for(Integer i=0;i<accResults.size();i++)
        {
            accMapCount.put(accResults[i].Id,accResults);
        }
    }  
    
    for(Account objAcc:Trigger.new)
    {
        if(Trigger.isInsert)
        {
            if(String.ValueOf(objAcc.Id)!=null && String.ValueOf(objAcc.Id)!='' )
            {
                List<Account> oldAccount=accMapCount.get(objAcc.Id);
                
                if(oldAccount!=null && oldAccount.size()>0)
                {
                    for(Account objAccnt:oldAccount)
                    {
                    if(objAccnt!=null)
                    {
                        
                        /* checking for billing state and city has no value for new and existing record */ 
                        
                        if(((objAccnt.BillingState==null || objAccnt.BillingState== '' )&& (objAccnt.BillingCity ==null || objAccnt.BillingCity ==''))&&((objAcc.BillingState==null || objAcc.BillingState == '') && (objAcc.BillingCity == null || objAcc.BillingCity == '')))
                        
                         {System.debug('objAcc----'+objAcc);
                           
                           /*  if name and phone values are equal */
                               if(objAcc.Name!= null && objAcc.Name == objAccnt.Name && objAcc.Phone == objAccnt.Phone && objAcc.Phone != null)
                                {    System.debug('objAcc-00---'+objAcc);
                                    //objAcc.addError('Account Already Exist');
                                }
                          /*  if name is equal and phone is not equal then inserting the parent */      
                                else if(objAcc.Name!= null && objAcc.Name == objAccnt.Name && objAcc.Phone != objAccnt.Phone && objAcc.Phone != null)
                                {
                                    objAcc.Name = objAcc.Name;
                                    objAcc.Phone = objAcc.Phone;
                                    objAcc.ParentId = objAccnt.Id;
                                }
                            }
                            
                          /*  if billing state and city of new has value and existing has no value*/
                          
                        else if (((objAcc.BillingState!= null || objAcc.BillingState!= '')&& (objAcc.BillingCity != null || objAcc.BillingCity != '')) && ((objAccnt.BillingState==null || objAccnt.BillingState== '' )&& (objAccnt.BillingCity ==null || objAccnt.BillingCity ==''))&& objAcc.Id != objAccnt.Id)
                       
                        {System.debug('objAcc---01-'+objAcc);
                            /*  name and phone of new is equal to existing */
                            if(objAcc.Name!= null && objAcc.Name == objAccnt.Name && objAcc.Phone == objAccnt.Phone && objAcc.Phone != null)
                             { System.debug('objAcc--02--'+objAcc);
                                 objAcc.Name = objAcc.Name;
                                    objAcc.Phone = objAcc.Phone;
                                    objAcc.ParentId = objAccnt.Id;
                                 objAccnt.BillingState =objAcc.BillingState;
                                 objAccnt.BillingCity =objAcc.BillingCity ;
                                 
                             }
                             /*  name is equal and phone is not equal*/
                            else if(objAcc.Name!= null && objAcc.Name == objAccnt.Name && objAcc.Phone != objAccnt.Phone && objAcc.Phone != null)
                                {System.debug('objAcc--03--'+objAcc);
                                    objAcc.Name = objAcc.Name;
                                    objAcc.Phone = objAcc.Phone;
                                    objAcc.BillingState = objAccnt.BillingState;
                                    objAcc.BillingCity =objAccnt.BillingCity; 
                                }
                 
                        }
                        
                        /* if new and existing has no value of city and state*/
                        
                         else if (((objAcc.BillingState == null || objAcc.BillingState == '')  && (objAcc.BillingCity == null || objAcc.BillingCity == ''))&& ((objAccnt.BillingState==null || objAccnt.BillingState== '' )&& (objAccnt.BillingCity ==null || objAccnt.BillingCity =='')) )
                        {System.debug('objAcc--04--'+objAcc);
                            if(objAcc.Name!= null && objAcc.Name == objAccnt.Name && objAcc.Phone == objAccnt.Phone && objAcc.Phone != null)
                             {     System.debug('objAcc--05--'+objAcc);
                                 //objAcc.addError('Account Already Exist');
                                 
                             }
                             else if(objAcc.Name!= null && objAcc.Name == objAccnt.Name && objAcc.Phone != objAccnt.Phone && objAcc.Phone != null)
                                {System.debug('objAcc--07--'+objAcc);
                                    objAcc.Name = objAcc.Name;
                                    objAcc.Phone = objAcc.Phone;
                                }
                        }
                      }
                    }
                    
                }
            }
        }
        else if(Trigger.isUpdate){
            if(String.ValueOf(objAcc.Id)!=null && String.ValueOf(objAcc.Id)!='' )
            {
                List<Account> oldAccount=accMapCount.get(objAcc.Id);
                
                if(oldAccount!=null && oldAccount.size()>0){
                    for(Account objAccnt:oldAccount)
                    {
                       if(objAccnt!=null)
                        {
                           if(((objAccnt.BillingState==null || objAccnt.BillingState== '' )&& (objAccnt.BillingCity ==null || objAccnt.BillingCity ==''))&&((objAcc.BillingState==null || objAcc.BillingState == '') && (objAcc.BillingCity == null || objAcc.BillingCity == '')) && objAcc.Id != objAccnt.Id)
                        
                         {System.debug('objAcc----'+objAcc);
                           
                               if(objAcc.Name!= null && objAcc.Name == objAccnt.Name && objAcc.Phone == objAccnt.Phone && objAcc.Phone != null && objAcc.Id != objAccnt.Id)
                                {    System.debug('objAcc-00---'+objAcc);
                                    //objAcc.addError('Account Already Exist');
                                }
                                else if(objAcc.Name!= null && objAcc.Name == objAccnt.Name && objAcc.Phone != objAccnt.Phone && objAcc.Phone != null && objAcc.Id != objAccnt.Id)
                                {
                                    objAcc.Name = objAcc.Name;
                                    objAcc.Phone = objAcc.Phone;
                                    objAcc.ParentId = objAccnt.Id;
                                }
                            }
                          
                        else if (((objAcc.BillingState!= null || objAcc.BillingState!= '')&& (objAcc.BillingCity != null || objAcc.BillingCity != '')) && ((objAccnt.BillingState==null || objAccnt.BillingState== '' )&& (objAccnt.BillingCity ==null || objAccnt.BillingCity ==''))&& objAcc.Id != objAccnt.Id)
                       
                        {System.debug('objAcc---01-'+objAcc);
                            if(objAcc.Name!= null && objAcc.Name == objAccnt.Name && objAcc.Phone == objAccnt.Phone && objAcc.Phone != null && objAcc.Id != objAccnt.Id)
                             { System.debug('objAcc--02--'+objAcc);
                                 objAcc.Name = objAcc.Name;
                                    objAcc.Phone = objAcc.Phone;
                                    objAcc.ParentId = objAccnt.Id;
                                 objAccnt.BillingState =objAcc.BillingState;
                                 objAccnt.BillingCity =objAcc.BillingCity ;
                                 
                             }
                            else if(objAcc.Name!= null && objAcc.Name == objAccnt.Name && objAcc.Phone != objAccnt.Phone && objAcc.Phone != null && objAcc.Id != objAccnt.Id)
                                {System.debug('objAcc--03--'+objAcc);
                                    objAcc.Name = objAcc.Name;
                                    objAcc.Phone = objAcc.Phone;
                                    objAcc.BillingState = objAccnt.BillingState;
                                    objAcc.BillingCity =objAccnt.BillingCity; 
                                }
                 
                        }
                         else if (((objAcc.BillingState == null || objAcc.BillingState == '')  && (objAcc.BillingCity == null || objAcc.BillingCity == ''))&& ((objAccnt.BillingState==null || objAccnt.BillingState== '' )&& (objAccnt.BillingCity ==null || objAccnt.BillingCity ==''))&& objAcc.Id != objAccnt.Id )
                        {System.debug('objAcc--04--'+objAcc);
                            if(objAcc.Name!= null && objAcc.Name == objAccnt.Name && objAcc.Phone == objAccnt.Phone && objAcc.Phone != null && objAcc.Id != objAccnt.Id )
                             {     System.debug('objAcc--05--'+objAcc);
                                 //objAcc.addError('Account Already Exist');
                                 
                             }
                             else if(objAcc.Name!= null && objAcc.Name == objAccnt.Name && objAcc.Phone != objAccnt.Phone && objAcc.Phone != null && objAcc.Id != objAccnt.Id)
                                {System.debug('objAcc--07--'+objAcc);
                                    objAcc.Name = objAcc.Name;
                                    objAcc.Phone = objAcc.Phone;
                                }
                        }
                      }
                    
                    }
                }
            }
        }
    }
               

}