trigger AccountCreateTrigger on Account (before insert, before update) {
    Set<String> accName = new Set<String>();
    Set<String> bCity = new Set<String>();
    Set<String> bState = new Set<String>();
    Set<String> bzip = new Set<String>();

    
    Set<String> existingAccName = new Set<String>();
    Set<String> existingCity = new Set<String>();
    Set<String> existingState = new Set<String>();
    Set<String> existingzip = new Set<String>();
       
    for (Account acc: Trigger.new)
    {
        accName.add(acc.Name);
        bCity.add(acc.BillingCity);
        bState.add(acc.BillingState);
        bzip.add(acc.BillingPostalCode);
    }
    
    for(Account accs :[SELECT Name, BillingCity, BillingState,BillingPostalCode FROM Account ])
    {
        existingAccName.add(accs.Name);
        existingCity.add(accs.BillingCity);
        existingState.add(accs.BillingState);
        existingzip.add(accs.BillingPostalCode);
    }

    for(Account acct: Trigger.new)    
    {  	
    	
	    	if(Trigger.isInsert)
	        {
	        	if(acct.BillingCity==null && acct.BillingState==null && acct.BillingPostalCode==null)
	        	{
				    if(existingAccName.contains(acct.Name))		    
				        acct.addError('Account name already exist.');  		    		    
				    else if(existingCity.contains(acct.BillingCity) && existingState.contains(acct.BillingState) && existingzip.contains(acct.BillingPostalCode))
				  
				        acct.addError('Account  with the same City, State, zip already exists. Please use existing account or contact your Salesforce Administrator for assistance.');
	        	system.debug('hiiiiiiiiiiiiiiiii'+acct.Name);
	        	system.debug('hiiiiiiiiiiiiiiiii'+acct.BillingState);
	        	system.debug('hiiiiiiiiiiiiiiiii'+acct.BillingCity);
	        	system.debug('hiiiiiiiiiiiiiiiii'+acct.BillingPostalCode);
	        	}
	        	 
	        }     
	       if(Trigger.isUpdate)
	       {
	       	if(existingAccName.contains(acct.Name))
	        acct.addError('Account name already exist.');    
	        else if(existingCity.contains(acct.BillingCity) && existingState.contains(acct.BillingState) && existingzip.contains(acct.BillingPostalCode))
	        acct.addError('Account  with the same City, State, zip already exists. Please use existing account or contact your Salesforce Administrator for assistance.'); 
	       }
    	
	    
    }
    }