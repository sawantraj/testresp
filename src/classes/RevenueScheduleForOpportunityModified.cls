/* This class is constructed to implement the edit & save functionality on 
	VisualForce page as well as multiple opportunityLineItem revenue schedules
	for respective opportunity line item of an opportunity which will be shown on 
	detail page of contract.
   
 * Revision History:
 *
 * Version       Date                 Author               Comments
 *  1.0         9/3/2013         Sachin Sankad.       Initial Draft
*/
public with sharing class RevenueScheduleForOpportunityModified 
{
	public List<OpportunityLineItemSchedule> lstOppLineItemSch{get;set;}
	private map<Id, List<OpportunityLineItemSchedule>> mapOppLineItemId_LISchList = new map<Id, List<OpportunityLineItemSchedule>>();
	private OpportunityLineItem objOpportunityLineItem;
	private set<Id> setOppLineItemId = new set<Id>(); 
	public boolean isDisplay {get;set;}
	public boolean isDisabled {get;set;}
	public boolean isSaveDisabled{get;set;}
	public list<lineItemSchedule> lineItemScheduleList {get;set;}
		
	public RevenueScheduleForOpportunityModified(ApexPages.StandardController stdController)
	{
		isDisplay = true;
		isDisabled = true;
		isSaveDisabled = true;
		lstOppLineItemSch = new List<OpportunityLineItemSchedule>();
		map <id,String> opplineItemNameIdMap = new Map<id,String>();
		
		map<Id, List<OpportunityLineItemSchedule>> mapOppLineItemSchedule = new map<Id, List<OpportunityLineItemSchedule>>();
		List<OpportunityLineItem> lstOpportunityLineItem;
		Contract objContract;
		
		Contract objContractRecord = (Contract)stdController.getRecord();
		
		if(objContractRecord.Id != null)
		{
			System.debug('objContractRecord'+objContractRecord);
			objContract = [Select Opportunity__c From Contract where Id =: objContractRecord.Id limit 1];
			System.debug('objContract'+objContract);
		}
		
		if(objContract.Opportunity__c != null)
		{
			lstOpportunityLineItem = [Select OpportunityId, Id, PricebookEntry.Name From OpportunityLineItem 
									  				Where OpportunityId =:objContract.Opportunity__c];
			System.debug('lstOpportunityLineItem in second if loop'+ lstOpportunityLineItem);
			for(OpportunityLineItem objOppLineItem : lstOpportunityLineItem)
			{
				System.debug('in for loop');
				System.debug('objOppLineItem in for loop'+objOppLineItem);
				setOppLineItemId.add(objOppLineItem.Id);
				System.debug('setOppLineItemId'+setOppLineItemId);
				opplineItemNameIdMap.put(objOppLineItem.Id,objOppLineItem.PricebookEntry.Name);
				System.debug('opplineItemNameIdMap in for loop'+opplineItemNameIdMap);
			}
			lstOppLineItemSch = getOpportunityLineItemSchedule();						  				
		}
		else
		{
			System.debug('in else');
			if(isDisabled)
				isDisabled = false;
    	  	ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Contract does not have an Opportunity and Opportunity Product Schedules to display.'));
		}
		//map <id,String> opplineItemNameIdMap = new Map<id,String>();//This code should be deleted while testing on sandbox.
		//System.debug('opplineItemNameIdMap'+opplineItemNameIdMap);
		System.debug('lstOpportunityLineItem'+lstOpportunityLineItem);
		
		/*for(OpportunityLineItem objOppLineItem : lstOpportunityLineItem) // This code should be deleted while testing on sandbox.
		{
			System.debug('in for loop');
			setOppLineItemId.add(objOppLineItem.Id);
			System.debug('setOppLineItemId'+setOppLineItemId);
			opplineItemNameIdMap.put(objOppLineItem.Id,objOppLineItem.PricebookEntry.Name);
		}*/
		
		//lstOppLineItemSch = getOpportunityLineItemSchedule(); //This code should be deleted while testing on sandbox.
		
		mapOppLineItemSchedule = getOppLineItemScheduleMap();
		
		lineItemScheduleList = new list<lineItemSchedule>();
		
		for (Id lineItemId : mapOppLineItemSchedule.keySet()) 
		{
			lineItemSchedule lineItemScheduleObject = new lineItemSchedule();
			if (opplineItemNameIdMap.containsKey(lineItemId))
			    lineItemScheduleObject.name = opplineItemNameIdMap.get(lineItemId);
			lineItemScheduleObject.lstInnerClassOppLineItmSch = mapOppLineItemSchedule.get(lineItemId);
			
			lineItemScheduleList.add(lineItemScheduleObject);
		}
		
		lstOppLineItemSch.clear();
		
		system.debug('mapOppLineItemSchedule ===========>'+mapOppLineItemSchedule);
		
		for(Id idOppLineItem : setOppLineItemId)
		{
			System.debug('idOppLineItem*********'+idOppLineItem);
			List<OpportunityLineItemSchedule> lstOppLSchedule = new List<OpportunityLineItemSchedule>();
			
			if(mapOppLineItemSchedule.get(idOppLineItem)!=null)
				lstOppLSchedule = mapOppLineItemSchedule.get(idOppLineItem);
			
			system.debug('lstOppLSchedule ===========>'+lstOppLSchedule);
			lstOppLineItemSch .addAll(lstOppLSchedule);
			system.debug('lstOppLineItemSch ===========>'+lstOppLineItemSch);
		}
			
	}
	
	private map<Id, List<OpportunityLineItemSchedule>> getOppLineItemScheduleMap()
	{
		for(OpportunityLineItemSchedule objLISch : lstOppLineItemSch)
		{
			system.debug('objLISch'+ objLISch);
			System.debug(' in for mapOppLineItemId_LISchList***********'+mapOppLineItemId_LISchList);
			if(mapOppLineItemId_LISchList.get(objLISch.OpportunityLineItemId) != null)
			{
				mapOppLineItemId_LISchList.get(objLISch.OpportunityLineItemId).add(objLISch);
				system.debug('Map in if loop of getOppLinteItemScheduleMap' + mapOppLineItemId_LISchList);
			}
			else
			{
				List<OpportunityLineItemSchedule> lstOLISch = new List<OpportunityLineItemSchedule>();
				lstOLISch.add(objLISch);
				system.debug('lstOLISch in getOppLineItemScheduleMap**********'+lstOLISch);
				mapOppLineItemId_LISchList.put(objLISch.OpportunityLineItemId, lstOLISch);
				system.debug('mapOppLineItemId_LISchList'+mapOppLineItemId_LISchList);
			}
		}
		System.debug('mapOppLineItemId_LISchList***********'+mapOppLineItemId_LISchList);
		return mapOppLineItemId_LISchList;
	}
	
	private List<OpportunityLineItemSchedule> getOpportunityLineItemSchedule()
	{
		System.debug('In getOpportunityLineItemSchedule function');
		List<OpportunityLineItemSchedule> lstLocalOppLineItemSch = new List<OpportunityLineItemSchedule>(); 
			if(setOppLineItemId != null)		
				lstLocalOppLineItemSch = [Select OpportunityLineItemId, ScheduleDate, Revenue, Description 
										  				 From OpportunityLineItemSchedule 
										  				 Where OpportunityLineItemId IN : setOppLineItemId//objOpportunityLineItem.Id
										  				 Order by ScheduleDate];
										  
				System.debug('lstLocalOppLineItemSch********************'+lstLocalOppLineItemSch);
				if(lstLocalOppLineItemSch.size() == 0)
				{
					if(isDisabled)
						isDisabled = false;
					ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'There are no revenue schedules for Opportunity Product to display.'));
				}
			return lstLocalOppLineItemSch;
	}
	
	public Pagereference editRevenueSchedule()
	{
		if(isDisplay)
		{
			isDisplay = false;
		}
		if(isSaveDisabled)
		{
			isSaveDisabled = false;
		}
		return null;
	}
	
	public Pagereference saveRevenueSchedule()
	{
		isDisplay=true;
		if(lstOppLineItemSch != null)
		{
			update lstOppLineItemSch;
		}
		if(isSaveDisabled == false)
		{
			isSaveDisabled = true;
		}
		return null;
	}
	
	public class lineItemSchedule
	{
		public String name {get;set;}
		public list<OpportunityLineItemSchedule> lstInnerClassOppLineItmSch {get;set;}
	}
}