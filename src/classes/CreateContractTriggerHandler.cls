/* This class is constructed to create the contract for an 
   opportunity whose stage will be updated to Closed Won.
   
 * Revision History:
 *
 * Version       Date                Author              Comments
 *  1.0        20/2/2013    njr        Initial Draft
*/
public with sharing class CreateContractTriggerHandler
{
    public List<Contract> contractsList;
    public List<Opportunity> opportunityEligibleList = new List<Opportunity>();
    
    public void onChangeStageUpdate(List<Opportunity> opportunityList, Map<Id, Opportunity> opportunityOldMap)
    {
        contractsList = new List<Contract>();
        List<Opportunity> opportunityEligibleList = new List<Opportunity>();
        Map<String,list<OpportunityLineItem>> opportunityToOppLineItemMap = new Map <String,list<OpportunityLineItem>>();
        
        for(Opportunity opportunity : opportunityList)
        {
            Opportunity opportunityOld = new Opportunity();
          if (opportunityOldMap.containsKey(opportunity.Id))
                opportunityOld = opportunityOldMap.get(opportunity.Id);
            if(opportunityOld.StageName != opportunity.StageName && opportunity.StageName =='Closed Won')
            {
                opportunityEligibleList.add(opportunity);
            }
        }
        
        list<OpportunityLineItem> opportunityLineItemList = [select id,CreatedDate,ServiceDate,OpportunityId from OpportunityLineItem 
                                                          where OpportunityId in :opportunityEligibleList];
                                                          
        for (OpportunityLineItem opplineItem : opportunityLineItemList)
        {
             if (opportunityToOppLineItemMap.containsKey(opplineItem.OpportunityId)) 
             {
                 opportunityToOppLineItemMap.get(opplineItem.OpportunityId).add(opplineItem);
             }
             else 
            {
                 opportunityToOppLineItemMap.put(opplineItem.OpportunityId,new list<OpportunityLineItem>{opplineItem});
            }
        }
        
        
	        for(Opportunity opportunity : opportunityEligibleList)
	        {
	            
	            if(opportunityToOppLineItemMap.containsKey(opportunity.id))
	            {
	                OpportunityLineItem oppLineItem = opportunityToOppLineItemMap.get(opportunity.id)[0];
	                Contract con = new Contract();
	                con.Opportunity__c = opportunity.Id;
	                if(opportunity.AccountId != null)
	                {
	                	con.AccountId = opportunity.AccountId;
	                	System.debug('Contract Account Id======='+con.AccountId);
	                }
	                else
	                {
	                	opportunity.addError('Account Name should not be empty.Please Select Account Name');	
	                }
	                con.Status ='Draft';
	                //con.CurrencyIsoCode =' NZD ';
	                //con.StartDate = Date.newInstance(opportunity.CreatedDate.year(),opportunity.CreatedDate.month(),opportunity.CreatedDate.day());
	                con.StartDate = oppLineItem.ServiceDate;
	                //con.Rolling_Contract__c = ' Rolling Contract ';
	                con.Description ='Test';
	                contractsList.add(con);
	                System.debug('contract created========');
	            }
	        }
        	try
        	{
        		insert contractsList;
        	}
        	Catch(Exception e)
        	{
        		e.getMessage();
        	}
    }
}