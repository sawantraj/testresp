public class FileUploader {
    public string fileData{get;set;}
    public Blob contentFile{get;set;}
    String[] fileRows;
    String[] firstRow;
    List<Account> accstoupload;
    
    
    public FileUploader() {
        accstoupload = new List<Account>();
        fileRows = new String[]{};
        firstRow = new String[]{};
    }
    public Pagereference ReadFile()
    {
        Integer descriptionColumn = 1;
        Integer priceBookColumn = 2;
        Integer productColumn = 3;
        Integer clientCountryColumn = 4;
        Integer accountManagerColumn = 5;
        Integer salesPersonColumn = 6;
        Integer allocationColumn = 7;
        Integer pipelineType = 8;
        Integer keyDeal = 9;
        map<String,list<Opportunity>> accountToOpportunityListMap = new map <String,list<Opportunity>>();
        map<String,Opportunity> nameToOpportunityMap = new map <String,Opportunity>();
        map<String,Account> nameToAccountMap = new map <String,Account>();
        map<String,String> opportunityToOwnerMap = new map<String,string>();
        map<String,String> opportunityToSalesPersonMap = new map<String,string>();
        map<String,Integer> opportunityToAllocationMap = new map<String,Integer>();
        map<String,id> nameToUserIdMap = new map<String,Id>();
        map<Opportunity,string> opportunityToPricebookMap = new map<Opportunity,string>();
        map<Opportunity,string> opportunityToProductMap = new map<Opportunity,string>();
        map<string,list<OpportunityLineItemSchedule>> opportunityToScheduleMap = new map<string,list<OpportunityLineItemSchedule>>();
        try {
            fileData = contentFile.toString();
        } catch (Exception ex) {
            system.debug('ex----->'+ex);
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured. Please check the template or try again later');
            ApexPages.addMessage(errormsg);
            return null;
        }
        
        system.debug('fileData---->'+fileData);
        fileRows = fileData.split('\n');
        for (string row:fileRows)
        system.debug('row---->'+row);
       // return null;
        
        if (fileRows != null && fileRows.size() > 0 ) {
            
            Integer row = 0;
            //For each row
            for (String rowData : fileRows) {
                
                //first row they are headers 
                if (row == 0) {
                    firstRow = fileRows[0].split(',');
                    for (string f : firstRow) 
                    	system.debug('f--->'+f);
                    	
                   // break;
                 
                } else {
                    String[] columData = rowData.split(',');
                    system.debug('columData---->'+columData);
                    if (row==3)return null;
                    continue;
                    Integer columNo = 1; 
                    //For each colum
                    Opportunity opportunity = new Opportunity ();
                    opportunity.StageName = 'Open';
                    opportunity.CloseDate = system.today();
                    Account account = new Account();
                    Decimal totalAmount = 0;
                    Date startYear,endYear;
                    string pricebookName,productName;
                    for (String colum : columData) {
                        //Stop processing if no datarow
                        if (colum == null || colum == '') {
                            columNo++;
                            continue;
                        }
                        system.debug('colum-->'+colum + 'columNo--->' + columNo);
                        //Else process further
                        
                        //1/Description colFor opportunity name and Account name
                        if (columNo == descriptionColumn) {
                            String accountName,opportunityName;
                            String[] values = colum.split('~');
                            if (values != null && values.size() > 0) {
                                accountName = values[0].trim();
                                opportunityName = values[1].trim();
                                
                                account.Name = accountName;
                                opportunity.Name = opportunityName;
                                
                                //Map account to opportunity
                                if (accountToOpportunityListMap.containsKey(accountName)) {
                                    accountToOpportunityListMap.get(accountName).add(opportunity);
                                } else {
                                    accountToOpportunityListMap.put(accountName,new list<Opportunity>{opportunity});
                                }
                                
                                nameToOpportunityMap.put(opportunityName,opportunity);
                                nameToAccountMap.put(accountName,account);
                            }
                        //PriceBook    
                        } else if (columNo == priceBookColumn) {
                        	pricebookName = colum.trim();
                           opportunityToPricebookMap.put(opportunity,colum.trim());
                        //Product 
                        } else if (columNo == productColumn) {
                        	productName = colum.trim();
                           opportunityToProductMap.put(opportunity,colum.trim());
                        //Client Country    
                        } else if (columNo == clientCountryColumn) {
                            opportunity.Oppotunity_Country__c = colum.trim();
                        //Manager    
                        } else if (columNo == accountManagerColumn) {
                           opportunityToOwnerMap.put(opportunity.Name,colum.trim());
                        //salesperson   
                        } else if (columNo == salesPersonColumn) {
                           opportunityToSalesPersonMap.put(opportunity.Name,colum.trim());
                        //Allocation  
                        } else if (columNo == allocationColumn) {
                           opportunityToAllocationMap.put(opportunity.Name,Integer.valueOf(colum.remove('%').trim()));
                        //pipelineType
                        } else if (columNo == pipelineType) {
                           opportunity.Pipeline_Type__c = colum;
                        //keydeal   
                        } else if (columNo == keyDeal) {
                            opportunity.Key_Deal__c = colum.trim();
                        } else {
                            colum = colum.trim();
                            system.debug('revenue colum--->'+colum);
                            if (!colum.isAlpha()) {
                                Decimal amount = Decimal.valueOf(colum);
                                system.debug('firstRow[columNo-1]--->'+firstRow[columNo-1]+'----amount--->'+amount);
                                if (amount > 0.0) {
                                    totalAmount += amount;
                                    opportunity.Amount = totalAmount;
                                    
                                        String value = firstRow[columNo-1];
                                        List<String> dateString = value.split('-');
                                        string yearStringFull = dateString[0].trim();
                                        if (yearStringFull.length() == 2)
                                        	yearStringFull = '20'+dateString[0].trim();
                                        Integer yearString = Integer.ValueOf(yearStringFull);
                                        system.debug('yearString--->'+yearString);
                                        String monthString = dateString[1].toUpperCase();
                                        
                                        Integer month;
                                        if ('JANUARY'.contains(monthString))
                                            month = 1;
                                        else if ('FEBRUARY'.contains(monthString))
                                            month = 2;
                                        else if ('MARCH'.contains(monthString))
                                            month = 3;
                                        else if ('APRIL'.contains(monthString))
                                            month = 4;
                                        else if ('MAY'.contains(monthString))
                                            month = 5;
                                        else if ('JUNE'.contains(monthString))
                                            month = 6;
                                        else if ('JULY'.contains(monthString))
                                            month = 7;
                                        else if ('AUGUST'.contains(monthString))
                                            month = 8;
                                        else if ('SEPTEMBER'.contains(monthString))
                                            month = 9;
                                        else if ('OCTOBER'.contains(monthString))
                                            month = 10;
                                        else if ('NOVEMBER'.contains(monthString))
                                            month = 11;
                                        else if ('DECEMBER'.contains(monthString))
                                            month = 12;
                                           
                                        startYear = date.newInstance(yearString,month,1);       
                                       // endyear = date.newInstance(month,1,yearString);
                                       OpportunityLineItemSchedule schedule = new OpportunityLineItemSchedule ();
                                       system.debug('startYear--->'+startYear);
                                       schedule.scheduleDate = startYear;
                                       schedule.revenue = amount;
                                       schedule.type = 'Revenue';
                                       system.debug('schedule-creation-->'+schedule + '<---->' +opportunity);
                                       String key = pricebookName + '-' + productName;
                                       if (opportunityToScheduleMap.containsKey(key)) {
                                           opportunityToScheduleMap.get(key).add(schedule);
                                           system.debug('in if--->');
                                       } else {
                                           opportunityToScheduleMap.put(key,new list<OpportunityLineItemSchedule>{schedule});
                                           system.debug('in else--->');
                                       }
                                }
                            }
                            //break;
                        
                        }
                        
                        columNo++;
                    }
                    
                }
                row++;
                
            }
            
            //Account processing
            Account[] accountList = [
                             select id
                                 ,  Name
                               from Account
                              where Name in : nameToAccountMap.keySet()
            ];
            
            
            for (Account accountfetch : accountList) {
                if (nameToAccountMap.containsKey(accountfetch.Name))
                    nameToAccountMap.get(accountfetch.Name).id = accountfetch.id;
                
            }
            
            upsert nameToAccountMap.values();
            //get users
            
            User [] userList = [
                        select id
                            ,  Name
                          from User
                          where (Name in : opportunityToOwnerMap.values()
                             or Name in : opportunityToSalesPersonMap.values())
                            and IsActive = true
            ];
            
            for (User user : userList) {
                system.debug('user name --->'+user.Name);
                nameToUserIdMap.put(user.Name,user.Id);
            }
            //Opportunity Processing
            for (Account account : nameToAccountMap.values()) {
                if (accountToOpportunityListMap.containsKey(account.Name)) {
                    for (Opportunity opportunity : accountToOpportunityListMap.get(account.Name)) {
                        opportunity.AccountId = account.id;
                        String ownerName;
                        system.debug('opportunity.Name--->'+opportunity.Name);
                        if (opportunityToAllocationMap.containsKey(opportunity.Name)) {
                            Integer allocation = opportunityToAllocationMap.get(opportunity.Name);
                            system.debug('allocation--->'+allocation);
                            if (allocation < 50) {
                                if (opportunityToSalesPersonMap.containsKey(opportunity.Name))
                                    ownerName = opportunityToSalesPersonMap.get(opportunity.Name);
                            } else {
                                if (opportunityToOwnerMap.containsKey(opportunity.Name))
                                    ownerName = opportunityToOwnerMap.get(opportunity.Name);
                            }
                        } else {
                            if (opportunityToOwnerMap.containsKey(opportunity.Name))
                                ownerName = opportunityToOwnerMap.get(opportunity.Name);
                        }
                        system.debug('ownerName----->'+ownerName);
                        if (ownerName != null && ownerName != '') {
                            if (nameToUserIdMap.containsKey(ownerName))
                                opportunity.ownerId = nameToUserIdMap.get(ownerName);
                        }
                    }
                }
            }
            
            insert nameToOpportunityMap.values();
            
            
            //processing line items
            map<string,id> nameToPricebookEntryIdMap = new map<string,id>();
            PricebookEntry[] pricebookEntryList = [
                                           select id
                                               ,  Name
                                               ,  Pricebook2.Name
                                               ,  Product2.Name
                                             from PricebookEntry
                                            where Pricebook2.Name in : opportunityToPricebookMap.values()
                                              and Product2.Name in : opportunityToProductMap.values()
                                              and IsActive = true
            ];
            for (PricebookEntry pricebookEntry : pricebookEntryList) {
            
                system.debug('Pricebook2.Name ---Product2.Name' + Pricebook2.Name + '-' + Product2.Name + '-->' + pricebookEntry );
                nameToPricebookEntryIdMap.put(pricebookEntry.Pricebook2.Name + '-' + pricebookEntry.Product2.Name,PricebookEntry.id);
            }
            //
            map<string,id> nameToPricebookIdMap = new map<string,id>();
            Pricebook2[] priceBookList = [
                                 select id
                                     ,  Name
                                     ,  IsStandard
                                   from Pricebook2
                                  where Name in : opportunityToPricebookMap.values()
                                     or IsStandard = true
            ];
            Pricebook2 pricebookStandard;
            for (Pricebook2 pricebook : priceBookList) {
                nameToPricebookIdMap.put(pricebook.Name,pricebook.id);
                if (pricebook.IsStandard)
                    pricebookStandard = pricebook;
            }
            if (pricebookStandard == null) {
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Please define standard price book');
                ApexPages.addMessage(errormsg);
                return null;
            }
            //
            map<string,id> nameToProductIdMap = new map<string,id>();
            Product2[] productList = [
                              select id
                                  ,  Name
                                from Product2
                               where Name in : opportunityToProductMap.values()
            ];
            
            for (Product2 product : productList) {
                nameToProductIdMap.put(product.Name,product.id);
            }
            
            map<string,OpportunityLineItem> opportunityLineItemInsertMap = new map<string,OpportunityLineItem>();
            map<string,PricebookEntry> pricebookEntryInsertMap = new map<string,PricebookEntry>();
            map<string,Product2> productInsertMap = new map<string,Product2>();
            map<string,Pricebook2> pricebookInsertMap = new map<string,Pricebook2>();
            
            for (Opportunity opportunity : nameToOpportunityMap.values()) {
                String pricebookName,productName;
                if (opportunityToPricebookMap.containsKey(opportunity))
                    pricebookName = opportunityToPricebookMap.get(opportunity);
                    
                if (opportunityToProductMap.containsKey(opportunity))
                    productName = opportunityToProductMap.get(opportunity);
                //if both productName and pricebookName both present then processid further.
                if (productName != null && productName != '' && pricebookName != null && pricebookName != '') {
                    opportunityLineItemInsertMap.put(pricebookName + '-' + productName,new OpportunityLineItem(Description = productName,OpportunityId = opportunity.id,Quantity = 1,TotalPrice = 0));
                    if (nameTopricebookEntryIdMap.containsKey(pricebookName + '-' + productName)) {
                        system.debug('pricebook entry found');
                        opportunityLineItemInsertMap.get(pricebookName + '-' + productName).PricebookEntryId = nameTopricebookEntryIdMap.get(pricebookName + '-' + productName);
                    } else {
                        pricebookEntryInsertMap.put(pricebookName + '-' + productName,new PricebookEntry(UnitPrice = 0,IsActive = true));
                        if (nameToPricebookIdMap.containsKey(pricebookName) && nameToProductIdMap.containsKey(productName)) {
                            PricebookEntry pricebookEntry = pricebookEntryInsertMap.get(pricebookName + '-' + productName);
                            pricebookEntry.Pricebook2Id = nameToPricebookIdMap.get(pricebookName);
                            pricebookEntry.Product2Id = nameToProductIdMap.get(productName);
                        } else if (nameToPricebookIdMap.containsKey(pricebookName) && !nameToProductIdMap.containsKey(productName)) {
                            PricebookEntry pricebookEntry = pricebookEntryInsertMap.get(pricebookName + '-' + productName);
                            pricebookEntry.Pricebook2Id = nameToPricebookIdMap.get(pricebookName);
                            productInsertMap.put(productName,new Product2 (Name = productName,IsActive = true,CanUseRevenueSchedule = true));
                        } else if (!nameToPricebookIdMap.containsKey(pricebookName) && nameToProductIdMap.containsKey(productName)) {
                            PricebookEntry pricebookEntry = pricebookEntryInsertMap.get(pricebookName + '-' + productName);
                            pricebookEntry.Product2Id = nameToProductIdMap.get(productName);
                            pricebookInsertMap.put(pricebookName,new Pricebook2(Name = pricebookName,IsActive = true));
                        } else {
                            productInsertMap.put(productName,new Product2 (Name = productName,IsActive = true));
                            pricebookInsertMap.put(pricebookName,new Pricebook2(Name = pricebookName,IsActive = true));
                        }
                    }
                }
            }
            
            if (pricebookInsertMap.values().size() != 0)
                insert pricebookInsertMap.values();
            if (productInsertMap.values().size() != 0)
                insert productInsertMap.values();
            List<PricebookEntry> pricebookEntryStandardList = new List<PricebookEntry>();     
            for (String key : pricebookEntryInsertMap.keySet()) {
                String[] values = key.split('-');
                string pricebookName = values[0];
                string productName = values[1];
                PricebookEntry pricebookEntry = pricebookEntryInsertMap.get(key);
                
                if (pricebookInsertMap.containsKey(pricebookName))
                    pricebookEntry.Pricebook2Id = pricebookInsertMap.get(pricebookName).id;
                if (productInsertMap.containsKey(productName)) {
                    pricebookEntry.Product2Id = productInsertMap.get(productName).id;
                    PricebookEntry pricebookEntryStandard = new PricebookEntry(UnitPrice = 0,IsActive = true);
                    pricebookEntryStandard.Product2Id = productInsertMap.get(productName).id;
                    pricebookEntryStandard.Pricebook2Id = pricebookStandard.id;
                    pricebookEntryStandardList.add(pricebookEntryStandard);
                }
            }
            if (pricebookEntryStandardList.size() != 0) 
                insert pricebookEntryStandardList;
            
            if (pricebookEntryInsertMap.values().size() != 0)
                insert pricebookEntryInsertMap.values();
                
            for (String key : opportunityLineItemInsertMap.keySet()) {
                if (pricebookEntryInsertMap.containsKey(key)) 
                    opportunityLineItemInsertMap.get(key).PricebookEntryId = pricebookEntryInsertMap.get(key).id;
            }
            
            if (opportunityLineItemInsertMap.values().size() != 0)
                insert opportunityLineItemInsertMap.values();
            list <OpportunityLineItemSchedule> scheduleInsertList = new list <OpportunityLineItemSchedule>();
            for (string key : opportunityToScheduleMap.keySet()) {
                list<OpportunityLineItemSchedule> scheduleList = opportunityToScheduleMap.get(key);
                system.debug('scheduleList--->'+scheduleList);
                String pricebookName,productName;
                if (opportunityLineItemInsertMap.containsKey(key)) {
                	OpportunityLineItem opportunityLineItem = opportunityLineItemInsertMap.get(key);
                	for (OpportunityLineItemSchedule schedule : scheduleList) { 
                		schedule.OpportunityLineItemId = opportunityLineItem.id;
                		scheduleInsertList.add(schedule);
                		system.debug('schedule--->'+schedule);
                	}
                }
            }
            
            if (scheduleInsertList.size() != 0)
            	insert scheduleInsertList;
        }
        ApexPages.Message infoMsg = new ApexPages.Message(ApexPages.severity.INFo,'All records has been inserted successfully');
        ApexPages.addMessage(infoMsg);
        return null;
    }
}