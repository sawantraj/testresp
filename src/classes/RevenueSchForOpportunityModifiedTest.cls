/* This test class is constructed to test the functionality of RevenueScheduleForOpportunityNew class.
   
 * Revision History:
 *
 * Version       Date                Author               Comments
 *  1.0        11/3/2013       Sachin Sankad         Initial Draft
*/

@isTest(seeAllData=true)
public with sharing class RevenueSchForOpportunityModifiedTest 
{
	static testmethod void insertTestData()
	{
		boolean isDisplay = true;
		List<OpportunityLineItemSchedule> lstOppLinItemSch = new List<OpportunityLineItemSchedule>();
		
		/* Inserting test account data */
		Account objAccount = new Account();
        objAccount.Name = 'Metservice test account';
        //objAccount.CurrencyIsoCode = 'NZD'; //To be uncommented while testing on sandbox.
        insert objAccount;
        
        /* Inserting test opportunity data */
         Opportunity objOpportunity = new Opportunity();
        objOpportunity.Name = 'test opportunity trigger';
        objOpportunity.AccountId = objAccount.Id;
        objOpportunity.CloseDate = date.parse('11/03/2013');
        objOpportunity.StageName = 'Product Trial';
        //objOpportunity.CurrencyIsoCode = 'NZD'; //To be uncommented while testing on sandbox.
        insert objOpportunity;
        
        String standardPriceBookId ='';
         
        /* Inserting test Product2 data */
        Product2 objProduct = new product2(Name='TestProduct', IsActive=true, 
        													  CanUseRevenueSchedule=true, 
        													  RevenueScheduleType='Repeat',
        													  RevenueInstallmentPeriod='Monthly',
        													  NumberOfRevenueInstallments=3
        													  );
        insert objProduct;
        
        Pricebook2 objPricebook = [Select Id,Name, IsActive  from Pricebook2 Where IsStandard=true];
        standardPriceBookId = objPricebook.Id; 
        
        PricebookEntry objPriceBookEntry = new PricebookEntry(Pricebook2Id=standardPriceBookId, 
        																					 Product2Id=objProduct.Id, 
        																					 UnitPrice=100, isActive=true); 
        insert objPriceBookEntry;
        
        OpportunityLineItem objOpportunityLineItem = new OpportunityLineItem(PricebookEntryId = objPriceBookEntry.Id, 
        																											  OpportunityId = objOpportunity.Id, 
        																											  Quantity = 1, TotalPrice = 100); 
        insert objOpportunityLineItem;
        
        Contract objContract = new Contract();
        objContract.Opportunity__c = objOpportunity.Id;
        objContract.AccountId = objAccount.Id;
        objContract.StartDate = date.parse('11/03/2013');
        //objContract.Rolling_Contract__c = 'Rolling Contract'; //To be uncommented while testing on sandbox.
        //objContract.CurrencyIsoCode = 'NZD'; //To be uncommented while testing on sandbox.
        insert objContract;
        
        OpportunityLineItemSchedule objOpportunityLineItemSchedule = new OpportunityLineItemSchedule();
        objOpportunityLineItemSchedule.OpportunityLineItemId = objOpportunityLineItem.Id;
        objOpportunityLineItemSchedule.Type ='Revenue';
        objOpportunityLineItemSchedule.ScheduleDate = date.parse('11/03/2013');
        objOpportunityLineItemSchedule.Revenue = 1000.0;
        objOpportunityLineItemSchedule.Description = 'test opportunity line item schedule - march';
        insert objOpportunityLineItemSchedule;
        
        lstOppLinItemSch.add(objOpportunityLineItemSchedule);
        
        ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(objContract);
        RevenueScheduleForOpportunityModified objRevenueScheduleModified = new RevenueScheduleForOpportunityModified(stdController);
        
        /*start test to test edit  & save functionality in RevenueScheduleForOpportunityNew class*/
        
        System.Test.startTest();
        	
        	objRevenueScheduleModified.editRevenueSchedule();
        	//System.debug('isDisplay--------'+objRevenueScheduleNew.isDisplay);
        	System.assertEquals(false,objRevenueScheduleModified.isDisplay);
        	        	
        	objRevenueScheduleModified.saveRevenueSchedule();
        	System.assertNotEquals(lstOppLinItemSch, objRevenueScheduleModified.lstOppLineItemSch);
        	System.assertEquals(objOpportunityLineItemSchedule.Revenue,objRevenueScheduleModified.lstOppLineItemSch[0].Revenue);
       
        System.Test.stopTest();
                
	}
	
	static testmethod void insertNegTestData()
	{
		boolean isDisplay = true;
		
		/* Inserting test account data */
		Account objAccount = new Account();
        objAccount.Name = 'Metservice test account';
        //objAccount.CurrencyIsoCode = 'NZD'; To be uncommented while testing on sandbox.
        insert objAccount;
                     
        /* Inserting test contract data */
        Contract objContract = new Contract();
        objContract.AccountId = objAccount.Id;
        objContract.StartDate = date.parse('11/03/2013');
        //objContract.Rolling_Contract__c = 'Rolling Contract'; //To be uncommented while testing on sandbox.
        //objContract.CurrencyIsoCode = 'NZD'; //To be uncommented while testing on sandbox.
        insert objContract;
                      
        ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(objContract);
        RevenueScheduleForOpportunityModified objRevenueScheduleModified = new RevenueScheduleForOpportunityModified(stdController);
		
		 System.Test.startTest();
        	  objRevenueScheduleModified.editRevenueSchedule();
        	  System.assertEquals(false,objRevenueScheduleModified.isDisabled);
          System.Test.stopTest();
	}
	
	static testmethod void checkOppLineItemSchedule()
	{
		boolean isDisplay = true;
		List<OpportunityLineItemSchedule> lstOppLinItemSch = new List<OpportunityLineItemSchedule>();
		/* Inserting test account data */
		Account objAccount = new Account();
        objAccount.Name = 'Metservice test account';
        //objAccount.CurrencyIsoCode = 'NZD'; To be uncommented while testing on sandbox.
        insert objAccount;
        
        /* Inserting test opportunity data */
         Opportunity objOpportunity = new Opportunity();
        objOpportunity.Name = 'test opportunity trigger';
        objOpportunity.AccountId = objAccount.Id;
        objOpportunity.CloseDate = date.parse('11/03/2013');
        objOpportunity.StageName = 'Product Trial';
        //objOpportunity.CurrencyIsoCode = 'NZD'; //To be uncommented while testing on sandbox.
        insert objOpportunity;
        
        String standardPriceBookId ='';
         
        /* Inserting test Product2 data */
        Product2 objProduct = new product2(Name='TestProduct', IsActive=true, 
        													  CanUseRevenueSchedule=true, 
        													  RevenueScheduleType='Repeat',
        													  RevenueInstallmentPeriod='Monthly',
        													  NumberOfRevenueInstallments=3
        													  );
        insert objProduct;
        
        Pricebook2 objPricebook = [Select Id,Name, IsActive  from Pricebook2 Where IsStandard=true];
        standardPriceBookId = objPricebook.Id; 
        
        PricebookEntry objPriceBookEntry = new PricebookEntry(Pricebook2Id=standardPriceBookId, 
        																					 Product2Id=objProduct.Id, 
        																					 UnitPrice=100, isActive=true); 
        insert objPriceBookEntry;
        
        OpportunityLineItem objOpportunityLineItem = new OpportunityLineItem(PricebookEntryId = objPriceBookEntry.Id, 
        																											  OpportunityId = objOpportunity.Id, 
        																											  Quantity = 1, TotalPrice = 100); 
        insert objOpportunityLineItem;
        
        Contract objContract = new Contract();
        objContract.Opportunity__c = objOpportunity.Id;
        objContract.AccountId = objAccount.Id;
        objContract.StartDate = date.parse('11/03/2013');
        //objContract.Rolling_Contract__c = 'Rolling Contract'; //To be uncommented while testing on sandbox.
        //objContract.CurrencyIsoCode = 'NZD'; //To be uncommented while testing on sandbox.
        insert objContract;
        
        ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(objContract);
        RevenueScheduleForOpportunityModified objRevenueScheduleModified = new RevenueScheduleForOpportunityModified(stdController);
		
		  System.Test.startTest();
        	  objRevenueScheduleModified.editRevenueSchedule();
        	  System.assertEquals(false,objRevenueScheduleModified.isDisabled);
          System.Test.stopTest();
	}
}