public class ScheduledRevenuesOnContractController {
	
	public List<OpportunityLineItem> oppLineItemList{get;set;}
	public List<OpportunityLineItemSchedule> oppLineItemSchedulesList{get;set;}
	
	public List<LineItemSchedulePerLI> LineItemSchedulePerLIList{get;set;}
	public List<showLineItemSchedules> showLineItemSchedulesList{get;set;}
	
	private string opportunityId{get;set;}
	
	public ScheduledRevenuesOnContractController(ApexPages.Standardcontroller stdController)
	{
		oppLineItemList = new List<OpportunityLineItem>();
		LineItemSchedulePerLIList = new List<LineItemSchedulePerLI>();
		
		Contract objContract = (Contract)stdController.getRecord();
		system.debug('=== : ' + objContract);
		opportunityId = (objContract.Opportunity__c != null) ? objContract.Opportunity__c : '';
		system.debug('=== : ' + objContract.Opportunity__c);
		
		oppLineItemList = [Select o.OpportunityId
								  , o.Id
								  , (Select Id
								  			, OpportunityLineItemId
								  			, Type
								  			, Revenue
								  			, Quantity
								  			, Description
								  			, ScheduleDate 
								  	 From OpportunityLineItemSchedules) 
						   From OpportunityLineItem o
						   where o.OpportunityId =: opportunityId];
		system.debug('=== Size : ' + oppLineItemList.size());
		for(OpportunityLineItem objOLI : oppLineItemList)
		{
			system.debug('### objOLI : ' + objOLI);
			showLineItemSchedulesList =  new List<showLineItemSchedules>();
			if(objOLI.OpportunityLineItemSchedules != null)
			for(OpportunityLineItemSchedule objOppLiSchedule : objOLI.OpportunityLineItemSchedules)
			{
				system.debug('###above continue');
				if(objOppLiSchedule==null)
					continue;
					
					system.debug('###continue');
				
				showLineItemSchedules  showLineItemSchedulesObj = new showLineItemSchedules(objOppLiSchedule
																	, objOppLiSchedule.OpportunityLineItemId
																	, objOppLiSchedule.Type
																	//, objOppLiSchedule.Revenue
																	//, objOppLiSchedule.Quantity
																	//, objOppLiSchedule.Description
																	, objOppLiSchedule.ScheduleDate
																	);
				system.debug('###');
				showLineItemSchedulesList.add(showLineItemSchedulesObj);
				system.debug('###1');
			}
			LineItemSchedulePerLIList.add(new LineItemSchedulePerLI(objOLI, showLineItemSchedulesList));
		}
	}
	
	public pageReference save()
	{
		return null;
	}
	
	public class LineItemSchedulePerLI{
		
		public OpportunityLineItem oppLineItemObj{get;set;}
		public List<showLineItemSchedules> showLineItemSchedulesList{get;set;}
		
		public LineItemSchedulePerLI(OpportunityLineItem oppLineItemObj, List<showLineItemSchedules> showLineItemSchedulesList)
		{
			this.oppLineItemObj = oppLineItemObj;
			this.showLineItemSchedulesList = showLineItemSchedulesList;
		}
	}
	public class showLineItemSchedules{
		
		public OpportunityLineItemSchedule objOppLISchedules{get;set;}
		public Id OpportunityLineItemId{get;set;}
		public string Type{get;set;}
		//public Decimal Revenue{get;set;}
		//public Decimal Quantity{get;set;}
		//public String Description{get;set;}
		public Date ScheduleDate{get;set;}
		
		public showLineItemSchedules(OpportunityLineItemSchedule objOppLISchedules
									 , string OpportunityLineItemId
									 , string Type
									 //, double Revenue
									 //, double Quantity
									 //, string Description
									 , date ScheduleDate
									 )
		{
			this.objOppLISchedules = objOppLISchedules;
			this.OpportunityLineItemId = OpportunityLineItemId;
			this.Type = Type;
			//this.Revenue = Revenue;
			//this.Quantity = Quantity;
			//this.Description = Description;
			this.ScheduleDate = ScheduleDate;
		}
	}
}