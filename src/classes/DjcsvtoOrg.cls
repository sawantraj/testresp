public with sharing class DjcsvtoOrg {
	public string nameFile{get;set;}
    public Blob contentFile{get;set;}
    String[] filelines = new String[]{};
    List<Account> accstoupload;
    
    public Pagereference ReadFile()
    {
      
        nameFile=contentFile.toString();
        filelines = nameFile.split('\n');
        accstoupload = new List<Account>();
        Map<String, Schema.SObjectField> fldObjMap = schema.SObjectType.Account.fields.getMap();
        List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();
        list<String> inputfields1=filelines[0].split(',');
        for (Integer i=1;i<filelines.size();i++)
        {
            //String[] inputvalues = new String[25];
            list<String> inputvalues=new list<string>();
            //system.debug('size is....'+ filelines[i].size());
            system.debug('i is....'+ i);
            inputvalues = filelines[i].split(',');
            
            Integer cnt=inputvalues.size();
           system.debug('inputvaluess....'+ inputvalues.size());
           system.debug('inputvaluess....'+ inputvalues);
           
         // system.debug('inputvaluess at 13 th index....'+ inputvalues[13]);
           
           
           Account a = new Account();
           
          
               for( Integer j=0; j<cnt;j++)
                 {
                   system.debug('value of j'+j);
                   system.debug('value of csv at '+j+' th index '+inputfields1[j]);
                   for(Schema.SObjectField s : fldObjMapValues)
                   {
                  String theName = s.getDescribe().getName();
                  system.debug('theName----'+theName);
                  String theLabel= s.getDescribe().getLabel();
                  //String type1=s.getDescribe().getType();
                  //system.debug('theLabel----'+theLabel);
                     Schema.DisplayType fielddataType = fldObjMap.get(string.valueOf(s)).getDescribe().getType();
                     //system.debug('datatype for....'+fielddataType);
                 // Boolean result = theName.equalsIgnoreCase(inputfields1[j]);
                
                  //system.debug('value of j'+j);
                  system.debug('j--->'+j+'theName ===>' +theName + 'filedname at j--->'+ inputfields1[j]);
                  if (theName.equalsIgnoreCase(inputfields1[j].trim()))
                   {
                      system.debug('value of j in if'+j+'===='+theName+'======'+inputfields1[j]);
                     if(string.valueOf(fielddataType).equalsIgnoreCase('currency'))
                     {
                     Decimal d = Decimal.valueOf(inputvalues[j]);
                      a.put(theName,d);
                      break;
                     }
                     else if(string.valueOf(fielddataType).equalsIgnoreCase('double'))
                     {
                     integer i1 = integer.valueOf(inputvalues[j]);
                      a.put(theName,i1);
                     break;
                     }
                     
                     else
                     {
                    a.put(theName,inputvalues[j]);
                    break;
                     }
                   }             
            // supportedFields.add(theName);

   // Continue building your dynamic query string
  
            }
            }
            
           // system.debug('account fields....'+supportedFields);
           
        
          

            accstoupload.add(a);
            system.debug('accstoupload.....'+accstoupload);
        }
        
        
        
        try{
          //system.debug('accstoupload....'+accstoupload);
        insert accstoupload;
        }
        catch (Exception e)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured. Please check the template or try again later');
            ApexPages.addMessage(errormsg);
        }    
        return null;
    }
    
    public List<Account> getuploadedAccounts()
    {
        if (accstoupload!= NULL)
            if (accstoupload.size() > 0)
                return accstoupload;
            else
                return null;                    
        else
            return null;
    }         

}