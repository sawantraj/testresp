public class OpportunityToXeroTriggerHandler {
 
    static Opportunity opportunity;
    static XeroLogin__c xlc;
    static sfXero.XeroCredentials xc;
    
    /** CREATE THE XERO CONTACT FROM THE OPPORTUNITY ACCOUNT **/
    public static sfXero.XeroContact accountToXeroContact(Opportunity opp) {
 
        //Find the account to convert from the opportunity.
        sfXero.XeroContact xContact = new sfXero.XeroContact();
        Account a = [Select Id, Name, AccountNumber, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, Phone
                     From Account
                     Where id = :opp.AccountId];
 		system.debug('---------'+a);
        //Add the details of the account to the new Xero Contact.
        xContact.contactNumber = a.AccountNumber;
        xContact.orgName = a.Name;
        xContact.isCustomer = true;
 
        //Add the phone from the account to the contact.
        sfXero.XeroContact.XeroAddress xAddress = new sfXero.XeroContact.XeroAddress();
                                       xAddress.addressType = sfXero.XeroContact.XeroAddressType.STREET;
                                       xAddress.AddressLine1 = a.BillingStreet;
                                       xAddress.city = a.BillingCity;
                                       xAddress.region = a.BillingState;
                                       xAddress.postalCode = a.BillingPostalCode;
                                       xAddress.country = a.BillingCountry;
        xContact.addresses = new List<sfXero.XeroContact.XeroAddress>();
        xContact.addAddress(xAddress);
 
        //Add the address from the account to the contact.
        sfXero.XeroContact.XeroPhone xPhone = new sfXero.XeroContact.XeroPhone();
                                     xPhone.phoneType = sfXero.XeroContact.XeroPhoneType.DEFAULTPHONE;
                                     xPhone.phoneNumber = a.phone;
        xContact.phones = new List<sfXero.XeroContact.XeroPhone>();
        xContact.addPhone(xPhone);
 
        //Return the converted contact.
        return xContact;
 
    }
 
    /** CREATE THE XERO LINE ITEMS FROM THE OPPORTUNITY **/
    public static List<sfXero.XeroLineItem> oppLineItemsToXeroLineItems(Opportunity opp) {
 		
        //Get all of the line items for the opportunity which need to be converted.
        List<OpportunityLineItem> olis = [Select Id, OpportunityId, Quantity, TotalPrice, ListPrice, Description, Tracking_Category__c
                                          From OpportunityLineItem
                                          Where OpportunityId = :opp.id];
 		
        List<sfXero.XeroLineItem> xLineItems = new List<sfXero.XeroLineItem>();
        for(OpportunityLineItem oli : olis) {
 
            
            sfXero.XeroLineItem xLineItem = new sfXero.XeroLineItem();
            //Turn the line opportunity line item into a line item.
            
            
            //TrackingCategories trackingCategories = new TrackingCategories();
            // TrackingCategories.GET();
            //Created an object of Tracking category and tracking option to pass tha values to tracking category
            list<sfxero.XeroTrackingCategory> track = new list<sfxero.XeroTrackingCategory>();
            List<sfXero.XeroTrackingCategory.XeroTrackingCategoryOption> trackOption = new List<sfXero.XeroTrackingCategory.XeroTrackingCategoryOption>();
            sfXero.XeroTrackingCategory.XeroTrackingCategoryOption optionObject = new sfXero.XeroTrackingCategory.XeroTrackingCategoryOption();
            optionObject.name = oli.Tracking_Category__c;
            trackOption.add(optionObject); 
            
            /* optionObject = new sfXero.XeroTrackingCategory.XeroTrackingCategoryOption();
            optionObject.name = 'Purchase';
            trackOption.add(optionObject);
            
            optionObject = new sfXero.XeroTrackingCategory.XeroTrackingCategoryOption();
            optionObject.name = 'Others';
            trackOption.add(optionObject);*/ 
            list<sfxero.XeroTrackingCategory> trackList = new list<sfxero.XeroTrackingCategory>();
            sfxero.XeroTrackingCategory trackObject = new sfxero.XeroTrackingCategory();
            trackObject.name = 'Development';
            trackObject.options = trackOption;            
            trackList.add(trackObject);
            
            xLineItem.description = oli.description;
            xLineItem.quantity = oli.quantity;
            xLineItem.accountCode = '00';
            xLineItem.trackingCategories = trackList;
            xLineItem.lineAmount = oli.listPrice*oli.quantity;
            //Add the line item to the list of line items.
            xLineItems.add(xLineItem);
        }
 
        //Return all of the line items.
        return xLineItems;
 
    }
    
   // @future
    public static void sendOpportunityToXero(Opportunity opportunity1) {
        
        opportunity = opportunity1;
        xlc = [Select Consumer_Key__c, Private_Key__c
                            From XeroLogin__c
                            Limit 1];
                            
        xc = new sfXero.XeroCredentials(xlc.Consumer_Key__c, '', xlc.Private_Key__c); 
        /** CREATE THE INVOICE FROM THE OPPORTUNITY **/
        List<sfXero.XeroInvoice> xListInvoices = new List<sfXero.XeroInvoice>();
        sfXero.XeroInvoice xInvoice = new sfXero.XeroInvoice();
                           xInvoice.invoiceType = sfXero.XeroInvoice.XeroInvoiceType.ACCREC;
                           xInvoice.contact = accountToXeroContact(opportunity);
                           xInvoice.issuedDate = system.today();
                           xInvoice.dueDate = opportunity.closeDate;
                           xInvoice.lineItems = oppLineItemsToXeroLineItems(opportunity);
 
        xListInvoices.add(xInvoice);
 
        
 
        sfXero.XeroInvoiceResponse xir = sfXero.XeroAPI.postInvoices(xListInvoices, xc);
        system.debug('---------'+xir);
        
 
    }
 
}