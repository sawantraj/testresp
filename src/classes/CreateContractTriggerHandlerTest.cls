/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=True)
Public class CreateContractTriggerHandlerTest {

    static testMethod void myUnitTest() {
        Account account = new Account();
        account.Name = 'test account';
        //account.CurrencyIsoCode = 'NZD';
        insert account;
        
        Opportunity opportunity = new Opportunity();
        opportunity.Name = 'test opportunity trigger';
        opportunity.AccountId = account.Id;
        opportunity.CloseDate = system.today();
        opportunity.StageName = 'Product Trial';
        //opportunity.ForecastCategory = 'Best Case';
        //opportunity.CurrencyIsoCode = 'NZD';
        insert opportunity;
        
        String standardPriceBookId ='';
                 
        Product2 product = new product2(Name='TestProduct', IsActive=true);
        insert product;
                
        Pricebook2 pricebook = [Select Id,Name, IsActive  from Pricebook2 Where IsStandard=true];
        standardPriceBookId = pricebook.Id; 
        //System.debug('*****PriceBook Id*****'+standardPriceBookId);
        
        
        PricebookEntry priceBookEntry = new PricebookEntry(Pricebook2Id=standardPriceBookId, Product2Id=product.Id, UnitPrice=100, isActive=true); 
        insert priceBookEntry;
        
        OpportunityLineItem opportunityLineItem = new OpportunityLineItem(PricebookEntryId = priceBookEntry.Id, OpportunityId = opportunity.Id, Quantity = 1, TotalPrice = 100); 
        insert opportunityLineItem;
        
        opportunityLineItem = new OpportunityLineItem(PricebookEntryId = priceBookEntry.Id, OpportunityId = opportunity.Id, Quantity = 2, TotalPrice = 100); 
        insert opportunityLineItem;
                
       // test.startTest();
            opportunity.StageName = 'Closed Won';
            update opportunity;
       // test.stopTest();
        
        Contract contract = [Select Id, Opportunity__c from Contract Where Opportunity__c =: opportunity.Id];
        System.assertEquals(contract.Opportunity__c,opportunity.Id);
        //System.debug('****Contract Opportunity Id**** '+contract.Opportunity__c);
        //System.debug('****Opportunity Id**** '+opportunity.Id);
    }
    
}