public with sharing class RevenueScheduleForOpportunity 
{
	public List<OpportunityLineItemSchedule> lstOppLineItemSch{get;set;} 
	public boolean isDisplay {get; set;}
	private Id contractId{get; set;}
	public List<RevenueSchedule> revenueScheduleList {get; set;} 
	private OpportunityLineItem objOpportunityLineItem;
	public Map<Id, OpportunityLineItemSchedule> opportunityLineItemScheduleMap = new Map<Id, OpportunityLineItemSchedule>();
	 
	public RevenueScheduleForOpportunity(ApexPages.StandardController stdController)
	{
		
		Date dt = Date.today().addDays(10);
		Id OLI = '00k90000007hXyh';
		OpportunityLineItemSchedule objSch = new OpportunityLineItemSchedule();
		objSch.Description = 'Test Description';
		objSch.OpportunityLineItemId = OLI;
		objSch.Quantity = 2;
		objSch.Revenue = 1500;
		objSch.ScheduleDate = dt;
		objSch.Type = 'Revenue';
		
		
	  	Contract objContract = (Contract)stdController.getRecord();
	  	contractId = objContract.Id;
	  	system.debug('==== objContract ' + objContract);
		objContract = [Select Opportunity__c From Contract where Id =:objContract.Id];
		system.debug('==== objContract 1 : ' + objContract);
		objOpportunityLineItem = [Select OpportunityId, Id 
								  From OpportunityLineItem 
							      Where OpportunityId =:objContract.Opportunity__c limit 1];
		
		lstOppLineItemSch = getOpportunityLineItemSchedules();
		system.debug('==== lstOppLineItemSch : ' + lstOppLineItemSch.size());
		FillRevenueSchedule(lstOppLineItemSch);
		system.debug('==== revenueScheduleList : ' + revenueScheduleList.size());
	}
	
	private List<OpportunityLineItemSchedule> getOpportunityLineItemSchedules()
	{
		if(lstOppLineItemSch==null)
		{
			opportunityLineItemScheduleMap = new Map<Id, OpportunityLineItemSchedule>([Select OpportunityLineItemId, ScheduleDate, Revenue, Description 
												 									   From OpportunityLineItemSchedule 
												 									   Where OpportunityLineItemId=:objOpportunityLineItem.Id
												 									   order by ScheduleDate]);
																										 
			//lstOppLineItemSch = opportunityLineItemScheduleMap.values(); 
			//lstOppLineItemSch.sort();
			
			return opportunityLineItemScheduleMap.values();
		}
		else
			return null;
	}
	
	public List<RevenueSchedule> FillRevenueSchedule(List<OpportunityLineItemSchedule> pOppLineItemScheduleList)
	{
		revenueScheduleList  = new List<RevenueSchedule>();
		RevenueSchedule objEdit;
		
		if(lstOppLineItemSch != null)
		{
			for(OpportunityLineItemSchedule objOpp : lstOppLineItemSch)
			{
				objEdit = new RevenueSchedule();
				objEdit.scheduleDate = (objOpp.ScheduleDate != null) ? objOpp.ScheduleDate : null;
				Date dt = objEdit.scheduleDate;//Date.newInstance(objEdit.scheduleDate.year, objEdit.scheduleDate.month, objEdit.scheduleDate.day);
				objEdit.revenue = (objOpp.Revenue != null) ? objOpp.Revenue : 0.0;
				objEdit.description = (objOpp.Description != null) ? objOpp.Description : '';
				objEdit.Id= objOpp.Id;
				revenueScheduleList.add(new RevenueSchedule(objEdit.scheduleDate, objEdit.revenue , objEdit.description,  objEdit.Id));
			}
		}
	  	return revenueScheduleList;
	} 
	
	public PageReference editOppLISchedule()
	{
		PageReference editPage = new PageReference('/apex/RevenueScheduleEdit?Id=' + contractId);
		editPage.setRedirect(true);
		return editPage;
	}
	
	public void saveRevenueSchedule()
	{
		system.debug('--------------- Exceuted');
		List<OpportunityLineItemSchedule> opportunityLineItemScheduleList = new List<OpportunityLineItemSchedule>();
		OpportunityLineItemSchedule objOppLineItmSc = new OpportunityLineItemSchedule(); 
		
		if(revenueScheduleList != null)
		{
			system.debug('==If  =Exceuted'+revenueScheduleList.size());
			for(RevenueSchedule objEditSc : revenueScheduleList)
			{
				system.debug('===For==========Exceuted');
				if(opportunityLineItemScheduleMap.containsKey(objEditSc.Id ))
				{
					System.debug('===If=========Exceuted');
					objOppLineItmSc = new OpportunityLineItemSchedule();
					objOppLineItmSc = opportunityLineItemScheduleMap.get(objEditSc.Id);
					objOppLineItmSc.ScheduleDate = objEditSc.scheduleDate;
					objOppLineItmSc.Revenue = objEditSc.revenue;
					
					objOppLineItmSc.Description = objEditSc.description;
				
					opportunityLineItemScheduleList.add(objOppLineItmSc);
				}
			}
			System.debug('---------------------opportunityLineItemScheduleList-----------'+opportunityLineItemScheduleList);
			if(opportunityLineItemScheduleList !=null)
			{
				System.debug(' Before Update ====>');
				List<Database.Saveresult> bdSave = database.update(opportunityLineItemScheduleList) ;
				System.debug(' After Update =====>' + bdSave);
			}
		}
	}
	

	public class RevenueSchedule
	{
		public Date scheduleDate{get;set;}
		public Double revenue{get;set;}
		public String description{get;set;}
		public String Id{get;set;}
		
		public RevenueSchedule(){}
		
		public RevenueSchedule(Date scheduleDate, Double revenue, String description, Id oppLISchedule)
		{
			//Date schDate = Date.newInstance(scheduleDate.year(), scheduleDate.month(), scheduleDate.day());
			this.scheduleDate = Date.newInstance(scheduleDate.year(), scheduleDate.month(), scheduleDate.day());//scheduleDate;
			this.revenue = revenue;
			this.description = description;
			this.id = oppLISchedule;
		}			
	}
		 
}