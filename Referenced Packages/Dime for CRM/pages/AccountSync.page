<apex:page standardcontroller="Account" extensions="sfxerope.AccountSync" action="{!init}" sidebar="false">
	
	<apex:includeScript value="{!$Resource.sfxerope__jquery}"/>
			
<style type="text/css">
	.requiredInput {
		position: absolute !important;
		height: auto !important;
	}
</style>
					
<script type="text/javascript">


/**
 * Handles the management and colorisation of two input fields.
 *
 * If two fields are different they are colored red. If they
 * have changed and since the last core refesh they are colored orange. 
 * If they are not equal they are colored red.
 * 
 * @author Jack David Galilee
 * @company Trineo Ltd
 * @date 2nd September 2011
 */

var managers = Array();
function CopyManager(leftInputField, leftInputButton, rightInputField, rightInputButton) {

	var me = this;

	// 
	var leftField = leftInputField;
	var leftButton = leftInputButton;
	var rightField = rightInputField;
	var rightButton = rightInputButton;
	
	// Left field managed by the copy manager.
	this.leftField = leftField;
	this.leftButton = leftButton;

	// Right field managed by the copy manager.
	this.rightField = rightField;
	this.rightButton = rightButton;

	// Return the value of the left field.
	this.getLeftValue = function() {

		if(me.leftField.getAttribute('type', 0) == 'checkbox') {
			return me.leftField.checked;
			
		} else {
			return me.leftField.value;
		}

	}

	// Return the value of the right field.
	this.getRightValue = function() {
		
		if(me.rightField.getAttribute('type', 0) == 'checkbox') {
			return me.rightField.checked;
			
		} else {
			return me.rightField.value;
		}
		
	}

	// Copy the value from the right field to the left field.
	this.copyLeft = function() {
		
		if(me.rightField.checked != undefined) {
			me.rightField.checked = me.leftField.checked;
		}
		
		if(me.rightField.value != undefined) {
			me.rightField.value = me.leftField.value;
		}
		
		me.colorFields();
		return false;

	}

	// Copy the value from the left field to the right field.
	this.copyRight = function() {
		
		if(me.leftField.checked != undefined) {
			me.leftField.checked = me.rightField.checked;
		}
		
		if(me.leftField.value != undefined) {
			me.leftField.value = me.rightField.value;
		}
		
		me.colorFields();
		return false;
		
	}

	// Return true if the values of the left and right field are equal.
	this.checkFields = function() {
		return (me.getLeftValue() == me.getRightValue());
	}

	// Color the fields depending on if they are equal or not.
	this.colorFields = function() {
		
		var colorName = '';
		
		if(!me.checkFields()) {
			colorName = '#FF9999';

		} else {
			colorName = 'none';

		}
		
		if(me.leftField.getAttribute('type', 0) != 'checkbox') {
			me.leftField.setAttribute('style', 'background-color: ' + colorName + ';')
			
		} else {
			me.leftField.setAttribute('style', ('outline: 3px solid ' + colorName + ';') )
		}
		
		if(me.rightField.getAttribute('type', 0) != 'checkbox') {
			me.rightField.setAttribute('style', 'background-color: ' + colorName + ';')
			
		} else {
			me.rightField.setAttribute('style', ('outline: 3px solid ' + colorName + ';') )
		}
		
		
	}

	this.id = managers.length;
	managers[this.id] = this;

	this.leftField.onkeyup = managers[this.id].colorFields;
	this.leftField.onchange = managers[this.id].colorFields; 
	this.leftButton.onclick = managers[this.id].copyLeft;

	this.rightField.onkeyup = managers[this.id].colorFields;
	this.rightField.onchange = managers[this.id].colorFields;
	this.rightButton.onclick = managers[this.id].copyRight;

	this.colorFields();

}

var copyManagers = Array();

function refreshManagers() {

	// Get all of the fields.
 	var sfAccountElements = $('.sfAccount');
	var xaContactElements = $('.xaContact');

	if(sfAccountElements.length == xaContactElements.length) {

		// Loop over all of the input fields and find the matching ones.
		for(var position = 0; position < sfAccountElements.length; position += 2) {
					
				// Record the two input fields.
				var sfInputField = sfAccountElements[position];
				var sfInputButton = sfAccountElements[position + 1];
	
				var xaInputField = xaContactElements[position];
				var xaInputButton = xaContactElements[position + 1];
					
				// Create a copy manager for the input fields.
				copyManagers.push(new CopyManager( sfInputField, sfInputButton, 
												   xaInputField, xaInputButton ));
	
		}
	}
}

function copyAllRight() {
	for( mngKey in managers) {
		var manager = managers[mngKey];
		manager.copyLeft();
	}
	return false;
}
	
function copyAllLeft() {
	for( mngKey in managers) {
		var manager = managers[mngKey];
		manager.copyRight();
	}
	return false;
}

</script>
    


	<apex:form >
        <apex:pageBlock id="Output"> 
					
			<apex:pageBlockButtons location="top" >
				<apex:commandButton value="Confirm All Changes" 
									action="{!confirmCurrentRecords}" 
									status="confirmingRecordStatus" 
									rendered="{!access.loggedIn}" />
				<apex:actionStatus startText="Updating Records ..." id="confirmingRecordStatus" />

				<apex:commandButton value="Create Contact In Xero" 
									action="{!createAccountOnXero}" 
									status="createMatch" 
									rendered="{!AND(access.loggedIn, !linkedWithXero)}" />
				<apex:actionStatus starttext="" id="createMatch" />

				<apex:commandButton value="Link with Account" 
									action="{!linkAccount}"
									rendered="{!AND(access.loggedIn, AND(hasGotProposals, !linkedWithXero))}" />

 				<apex:commandButton value="Unlink with Xero" 
 									action="{!unlinkAccount}" 
 									rendered="{!AND(access.loggedIn, AND(hasGotProposals, linkedWithXero))}" />

				<apex:commandButton value="Keep in Sync" 
									action="{!syncAccount}"
									rendered="{!AND(access.loggedIn, AND(!syncedWithXero, linkedWithXero))}" />
 				<apex:commandButton value="Stop Sync" 
 									action="{!unSyncAccount}" 
 									rendered="{!AND(access.loggedIn, AND(syncedWithXero, linkedWithXero))}" />
							
 				<apex:commandButton value="Back to Account" action="{!backtoAccount}" />
			</apex:pageBlockButtons>
	
   			<apex:pageMessages ></apex:pageMessages>
   			
        </apex:pageBlock>



	    <table style="width: 100%; vertical-align: top;">
	        <tr>
	        <td style="width: 50%; vertical-align: top;">
				<apex:pageBlock title="Salesforce" >
					
					<apex:pageBlockButtons location="both">
						
						<apex:commandButton value="Copy All Right" onclick="copyAllRight(); return false;"/>
						
					</apex:pageBlockButtons>
					
					<apex:pageBlockSection title="Salesforce Information" columns="1" collapsible="false">

		                <apex:pageBlockSectionItem rendered="{!(!isPersonAccount)}" >
		                    <apex:outputLabel value="Name" />
							<apex:outputPanel >
		                    	<apex:inputField styleclass="sfAccount Name" value="{!accountObject.Name}" />
		                    	<apex:commandButton onclick="return false;" styleclass="sfAccount Name Button" style="float: right;" value=">" />
	                    	</apex:outputPanel>
		                </apex:pageBlockSectionItem>
		                
		                <apex:pageBlockSectionItem rendered="{!isPersonAccount}" >
		                    <apex:outputLabel value="First Name" />
							<apex:outputPanel >
		                    	<apex:inputText styleclass="sfAccount FirstName" value="{!accountFirstName}" />
		                    	<apex:commandButton onclick="return false;" styleclass="sfAccount FirstName Button" style="float: right;" value=">" />
	                    	</apex:outputPanel>
		                </apex:pageBlockSectionItem>
		                
		                <apex:pageBlockSectionItem rendered="{!isPersonAccount}" >
		                    <apex:outputLabel value="Last Name" />
							<apex:outputPanel >
		                    	<apex:inputText styleclass="sfAccount Last Name" value="{!accountLastName}" />
		                    	<apex:commandButton onclick="return false;" styleclass="sfAccount LastName Button" style="float: right;" value=">" />
	                    	</apex:outputPanel>
		                </apex:pageBlockSectionItem>
		            
		                <apex:pageBlockSectionItem >
		                    <apex:outputLabel value="Account Id"/>
							<apex:pageBlockSectionItem >
		                    	<apex:outputField value="{!accountObject.Id}"/>
		                    </apex:pageBlockSectionItem>
		                </apex:pageBlockSectionItem>
		                
	                </apex:pageBlockSection>
		                
	                <apex:pageBlockSection title="Phone Numbers" columns="1" collapsible="false">
		                		                
		                <apex:pageBlockSectionItem >
		                    <apex:outputLabel value="Phone" />
		                    <apex:pageBlockSectionItem >
		                    	<apex:inputField styleclass="sfAccount Phone" value="{!accountObject.Phone}" />
		                    	<apex:commandButton onclick="return false;" styleclass="sfAccount Phone Button" style="float: right;" value=">" />
		                    </apex:pageBlockSectionItem>
		                </apex:pageBlockSectionItem>
		                
		                <apex:pageBlockSectionItem >
		                    <apex:outputLabel value="Fax" />
		                    <apex:pageBlockSectionItem >
		                    	<apex:inputField styleclass="sfAccount Fax" value="{!accountObject.Fax}" />
		                    	<apex:commandButton onclick="return false;" styleclass="sfAccount Fax Button" style="float: right;" value=">" />
		                    </apex:pageBlockSectionItem>
		                </apex:pageBlockSectionItem>
		                
	                </apex:pageBlockSection>
	                
	                <apex:pageBlockSection title="Addresses" columns="1" collapsible="false">
	                
		                <apex:pageBlockSectionItem >
		                    <apex:outputLabel value="Billing Street" />
		                    <apex:pageBlockSectionItem >
		                    	<apex:inputField styleclass="sfAccount Billing_Street" value="{!accountObject.BillingStreet}" />
		                    	<apex:commandButton onclick="return false;" styleclass="sfAccount Billing_Street Button" style="float: right;" value=">" />
		                    </apex:pageBlockSectionItem>
		                </apex:pageBlockSectionItem>
		                
		                <apex:pageBlockSectionItem >
		                    <apex:outputLabel value="Billing State" />
		                    <apex:pageBlockSectionItem >
		                    	<apex:inputField styleclass="sfAccount Billing_State" value="{!accountObject.BillingState}" />
		                    	<apex:commandButton onclick="return false;" styleclass="sfAccount Billing_State Button" style="float: right;" value=">" />
		                    </apex:pageBlockSectionItem>
		                </apex:pageBlockSectionItem>
		                
		                <apex:pageBlockSectionItem >
		                    <apex:outputLabel value="Billing Country" />
		                    <apex:pageBlockSectionItem >
		                    	<apex:inputField styleclass="sfAccount Billing_Country" value="{!accountObject.BillingCountry}" />
		                    	<apex:commandButton onclick="return false;" styleclass="sfAccount Billing_Country Button" style="float: right;" value=">" />
		                    </apex:pageBlockSectionItem>
		                </apex:pageBlockSectionItem>
		                
		                <apex:pageBlockSectionItem >
		                    <apex:outputLabel value="Billing PostalCode" />
		                    <apex:pageBlockSectionItem >
		                    	<apex:inputField styleclass="sfAccount Billing_PostalCode" value="{!accountObject.BillingPostalCode}" />
		                    	<apex:commandButton onclick="return false;" styleclass="sfAccount Billing_PostalCode Button" style="float: right;" value=">" />
		                    </apex:pageBlockSectionItem>
		                </apex:pageBlockSectionItem>
		                
		                <apex:pageBlockSectionItem >
		                    <apex:outputLabel value="Shipping Street" />
		                    <apex:pageBlockSectionItem >
		                    	<apex:inputField styleclass="sfAccount Billing_Street" value="{!accountObject.ShippingStreet}" />
		                    	<apex:commandButton onclick="return false;" styleclass="sfAccount Billing_Street Button" style="float: right;" value=">" />
		                    </apex:pageBlockSectionItem>
		                </apex:pageBlockSectionItem>
		                
		                <apex:pageBlockSectionItem >
		                    <apex:outputLabel value="Shipping State" />
		                    <apex:pageBlockSectionItem >
		                    	<apex:inputField styleclass="sfAccount Billing_State" value="{!accountObject.ShippingState}" />
		                    	<apex:commandButton onclick="return false;" styleclass="sfAccount Billing_State Button" style="float: right;" value=">" />
		                    </apex:pageBlockSectionItem>
		                </apex:pageBlockSectionItem>
		                
		                <apex:pageBlockSectionItem >
		                    <apex:outputLabel value="Shipping Country" />
		                    <apex:pageBlockSectionItem >
		                    	<apex:inputField styleclass="sfAccount Billing_Country" value="{!accountObject.ShippingCountry}" />
		                    	<apex:commandButton onclick="return false;" styleclass="sfAccount Billing_Country Button" style="float: right;" value=">" />
		                    </apex:pageBlockSectionItem>
		                </apex:pageBlockSectionItem>
		                
		                <apex:pageBlockSectionItem >
		                    <apex:outputLabel value="Shipping PostalCode" />
		                    <apex:pageBlockSectionItem >
		                    	<apex:inputField styleclass="sfAccount Billing_PostalCode" value="{!accountObject.ShippingPostalCode}" />
		                    	<apex:commandButton onclick="return false;" styleclass="sfAccount Billing_PostalCode Button" style="float: right;" value=">" />
		                    </apex:pageBlockSectionItem>
		                </apex:pageBlockSectionItem>
		                
		                <apex:pageBlockSectionItem >
		                    <apex:outputLabel value="Tax Number" />
		                    <apex:pageBlockSectionItem >
		                    	<apex:inputField styleclass="sfAccount Billing_PostalCode" value="{!accountObject.sfxerope__Tax_Number__c}" />
		                    	<apex:commandButton onclick="return false;" styleclass="sfAccount Billing_PostalCode Button" style="float: right;" value=">" />
		                    </apex:pageBlockSectionItem>
		                </apex:pageBlockSectionItem>
	                
	                </apex:pageBlockSection>

				</apex:pageBlock>
 			</td>
 
 			<td style="width: 50%; vertical-align: top;">
				<apex:pageBlock title="Xero {!IF(hasGotProposals, currentProposalPosStr, '')}" id="XeroContactBlock">
					
					<!-- If they are connected to Xero then give them the information they need to do comparisons. -->
					<!-- This assumes that they have been authenticated properly with Xero. -->
					
					<apex:outputLabel value="No Xero contact to display." rendered="{!NOT(AND(access.loggedIn, hasGotProposals))}" />
					
					<apex:pageBlockSection title="Xero Information" columns="1" collapsible="false" rendered="{!AND(access.loggedIn, hasGotProposals)}">
						
		                <apex:pageBlockSectionItem rendered="{!NOT(isPersonAccount)}" >
		                    <apex:outputLabel value="Name" />
							<apex:outputPanel >
		                    	<apex:inputField styleclass="xaContact Name" value="{!currentProposal.Name}"/>
		                    	<apex:commandButton onclick="return false;" styleclass="xaContact Name Button" style="float: right;" value="<" />
	                    	</apex:outputPanel>
		                </apex:pageBlockSectionItem>
		                
		                <apex:pageBlockSectionItem rendered="{!isPersonAccount}" >
		                    <apex:outputLabel value="First Name" />
							<apex:outputPanel >
		                    	<apex:inputText styleclass="xaContact FirstName" value="{!proposalFirstName}" />
		                    	<apex:commandButton onclick="return false;" styleclass="xaContact FirstName Button" style="float: right;" value=">" />
	                    	</apex:outputPanel>
		                </apex:pageBlockSectionItem>
		                
		                <apex:pageBlockSectionItem rendered="{!isPersonAccount}" >
		                    <apex:outputLabel value="Last Name" />
							<apex:outputPanel >
		                    	<apex:inputText styleclass="xaContact LastName" value="{!proposalLastName}" />
		                    	<apex:commandButton onclick="return false;" styleclass="xaContact LastName Button" style="float: right;" value=">" />
	                    	</apex:outputPanel>
		                </apex:pageBlockSectionItem>
		                
		                <apex:pageBlockSectionItem >
		                    <apex:outputLabel value="Xero Id"/>
							<apex:pageBlockSectionItem >
		                    	<apex:outputField value="{!currentProposal.sfxerope__Xero_Id__c}"/>
		                    </apex:pageBlockSectionItem>
		                </apex:pageBlockSectionItem>
		                
	                </apex:pageBlockSection>
		                
	                <apex:pageBlockSection title="Phone Numbers" columns="1" collapsible="false" rendered="{!AND(access.loggedIn, hasGotProposals)}">
		                		                
		                <apex:pageBlockSectionItem >
		                    <apex:outputLabel value="Phone" />
		                    <apex:pageBlockSectionItem >
		                    	<apex:inputField styleclass="xaContact Phone" value="{!currentProposal.Phone}" />
		                    	<apex:commandButton onclick="return false;" styleclass="xaContact Phone Button" style="float: right;" value="<" />
		                    </apex:pageBlockSectionItem>
		                </apex:pageBlockSectionItem>
		                
		                <apex:pageBlockSectionItem >
		                    <apex:outputLabel value="Fax" />
		                    <apex:pageBlockSectionItem >
		                    	<apex:inputField styleclass="xaContact Fax" value="{!currentProposal.Fax}" />
		                    	<apex:commandButton onclick="return false;" styleclass="xaContact Fax Button" style="float: right;" value="<" />
		                    </apex:pageBlockSectionItem>
		                </apex:pageBlockSectionItem>
		                
	                </apex:pageBlockSection>
	                
	                <apex:pageBlockSection title="Addresses" columns="1" collapsible="false" rendered="{!AND(access.loggedIn, hasGotProposals)}">
	                
		                <apex:pageBlockSectionItem >
		                    <apex:outputLabel value="Billing Street" />
		                    <apex:pageBlockSectionItem >
		                    	<apex:inputField styleclass="xaContact Billing_Street" value="{!currentProposal.BillingStreet}" />
		                    	<apex:commandButton onclick="return false;" styleclass="xaContact Billing_Street Button" style="float: right;" value="<" />
		                    </apex:pageBlockSectionItem>
		                </apex:pageBlockSectionItem>
		                
		                <apex:pageBlockSectionItem >
		                    <apex:outputLabel value="Billing State" />
		                    <apex:pageBlockSectionItem >
		                    	<apex:inputField styleclass="xaContact Billing_State" value="{!currentProposal.BillingState}" />
		                    	<apex:commandButton onclick="return false;" styleclass="xaContact Billing_State Button" style="float: right;" value="<" />
		                    </apex:pageBlockSectionItem>
		                </apex:pageBlockSectionItem>
		                
		                <apex:pageBlockSectionItem >
		                    <apex:outputLabel value="Billing Country" />
		                    <apex:pageBlockSectionItem >
		                    	<apex:inputField styleclass="xaContact Billing_Country" value="{!currentProposal.BillingCountry}" />
		                    	<apex:commandButton onclick="return false;" styleclass="xaContact Billing_Country Button" style="float: right;" value="<" />
		                    </apex:pageBlockSectionItem>
		                </apex:pageBlockSectionItem>
		                
		                <apex:pageBlockSectionItem >
		                    <apex:outputLabel value="Billing PostalCode" />
		                    <apex:pageBlockSectionItem >
		                    	<apex:inputField styleclass="xaContact Billing_PostalCode" value="{!currentProposal.BillingPostalCode}" />
		                    	<apex:commandButton onclick="return false;" styleclass="xaContact Billing_PostalCode Button" style="float: right;" value="<" />
		                    </apex:pageBlockSectionItem>
		                </apex:pageBlockSectionItem>
		                
		                <apex:pageBlockSectionItem >
		                    <apex:outputLabel value="Shipping Street" />
		                    <apex:pageBlockSectionItem >
		                    	<apex:inputField styleclass="xaContact Billing_Street" value="{!currentProposal.ShippingStreet}" />
		                    	<apex:commandButton onclick="return false;" styleclass="xaContact Billing_Street Button" style="float: right;" value="<" />
		                    </apex:pageBlockSectionItem>
		                </apex:pageBlockSectionItem>
		                
		                <apex:pageBlockSectionItem >
		                    <apex:outputLabel value="Shipping State" />
		                    <apex:pageBlockSectionItem >
		                    	<apex:inputField styleclass="xaContact Billing_State" value="{!currentProposal.ShippingState}" />
		                    	<apex:commandButton onclick="return false;" styleclass="xaContact Billing_State Button" style="float: right;" value="<" />
		                    </apex:pageBlockSectionItem>
		                </apex:pageBlockSectionItem>
		                
		                <apex:pageBlockSectionItem >
		                    <apex:outputLabel value="Shipping Country" />
		                    <apex:pageBlockSectionItem >
		                    	<apex:inputField styleclass="xaContact Billing_Country" value="{!currentProposal.ShippingCountry}" />
		                    	<apex:commandButton onclick="return false;" styleclass="xaContact Billing_Country Button" style="float: right;" value="<" />
		                    </apex:pageBlockSectionItem>
		                </apex:pageBlockSectionItem>
		                
		                <apex:pageBlockSectionItem >
		                    <apex:outputLabel value="Shipping PostalCode" />
		                    <apex:pageBlockSectionItem >
		                    	<apex:inputField styleclass="xaContact Billing_PostalCode" value="{!currentProposal.ShippingPostalCode}" />
		                    	<apex:commandButton onclick="return false;" styleclass="xaContact Billing_PostalCode Button" style="float: right;" value="<" />
		                    </apex:pageBlockSectionItem>
		                </apex:pageBlockSectionItem>
		                
		                <apex:pageBlockSectionItem >
		                    <apex:outputLabel value="Tax Number" />
		                    <apex:pageBlockSectionItem >
		                    	<apex:inputField styleclass="xaContact Billing_PostalCode" value="{!currentProposal.sfxerope__Tax_Number__c}" />
		                    	<apex:commandButton onclick="return false;" styleclass="xaContact Billing_PostalCode Button" style="float: right;" value="<" />
		                    </apex:pageBlockSectionItem>
		                </apex:pageBlockSectionItem>
	                
	                </apex:pageBlockSection>
	                 
	                
					<apex:pageBlockButtons location="both" rendered="{!AND(access.loggedIn, hasGotProposals)}">

						
						<apex:commandButton value="Copy All Left" onclick="copyAllLeft(); return false;" disabled="{!OR(NOT(access.loggedIn), NOT(hasGotProposals))}" />
        					 				
						
					</apex:pageBlockButtons>
					
				</apex:pageBlock>
			</td>
			</tr>
			</table>
			<script type="text/javascript">
				refreshManagers();
			</script>

</apex:form>





</apex:page>